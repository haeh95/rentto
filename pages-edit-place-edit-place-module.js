(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-place-edit-place-module"],{

/***/ "./src/app/pages/edit-place/edit-place.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/edit-place/edit-place.module.ts ***!
  \*******************************************************/
/*! exports provided: EditPlacePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPlacePageModule", function() { return EditPlacePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _edit_place_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-place.page */ "./src/app/pages/edit-place/edit-place.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _edit_place_page__WEBPACK_IMPORTED_MODULE_5__["EditPlacePage"]
    }
];
var EditPlacePageModule = /** @class */ (function () {
    function EditPlacePageModule() {
    }
    EditPlacePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_edit_place_page__WEBPACK_IMPORTED_MODULE_5__["EditPlacePage"]]
        })
    ], EditPlacePageModule);
    return EditPlacePageModule;
}());



/***/ }),

/***/ "./src/app/pages/edit-place/edit-place.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/edit-place/edit-place.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark></app-header-dark>\n<ion-content>\n  <ion-grid fixed>\n    <ion-row padding-vertical>\n      <ion-col no-padding>\n        <h1>{{place.name}}</h1>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <form novalidate #editForm=\"ngForm\" (ngSubmit)=\"send()\">\n    <div class=\"bg\">\n      <ion-grid fixed>\n        <ion-row align-items-stretch>\n          <ion-col no-padding size=\"12\" size-md=\"8\">\n            <ion-row padding-vertical>\n              <ion-col no-padding size=\"11\">\n                <ion-item lines=\"none\" color=\"noColor\">\n                  <ion-label position=\"stacked\" no-margin color=\"lightDark\">Titulo</ion-label>\n                  <ion-input [(ngModel)]=\"place.name\" name=\"name\" placeholder=\"Titulo del espacio\" required>\n                  </ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-row padding-bottom>\n              <ion-col no-padding size=\"4\">\n                <ion-item lines=\"none\" color=\"noColor\">\n                  <ion-label position=\"stacked\" no-margin color=\"lightDark\">Precio base ({{place.price_duration}})</ion-label>\n                  <ion-input [(ngModel)]=\"place.price\" name=\"price\" placeholder=\"$\" type=\"number\" required>\n                  </ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n            <ion-text text-left color=\"lightDark\">\n              <p no-margin><small><strong>Tipo de espacio</strong></small></p>\n            </ion-text>\n            <ion-radio-group [(ngModel)]=\"place.type_id\" (ionChange)=\"listServices()\" name=\"type_id\" required>\n              <ion-row class=\"ion-hide-md-down\">\n                <ion-col no-padding padding-left>\n                  <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                    <ion-radio slot=\"start\" [value]=\"1\"></ion-radio>\n                    <ion-label><ion-icon src=\"./assets/svg/type_1.svg\"></ion-icon> Parqueadero</ion-label>\n                  </ion-item>\n                </ion-col>\n                <ion-col no-padding padding-horizontal>\n                  <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                    <ion-radio slot=\"start\" [value]=\"2\"></ion-radio>\n                    <ion-label><ion-icon src=\"./assets/svg/type_2.svg\"></ion-icon> Espacio doméstico</ion-label>\n                  </ion-item>\n                </ion-col>\n                <ion-col no-padding padding-horizontal>\n                  <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                    <ion-radio slot=\"start\" [value]=\"3\"></ion-radio>\n                    <ion-label><ion-icon src=\"./assets/svg/type_3.svg\"></ion-icon> Espacio comercial</ion-label>\n                  </ion-item>\n                </ion-col>\n                <ion-col no-padding>\n                  <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                    <ion-radio slot=\"start\" [value]=\"4\"></ion-radio>\n                    <ion-label><ion-icon src=\"./assets/svg/type_4.svg\"></ion-icon> Equipaje</ion-label>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n              <ion-row class=\"ion-hide-md-up\">\n                  <ion-col>\n                    <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                      <ion-radio slot=\"start\" [value]=\"1\"></ion-radio>\n                      <ion-label><ion-icon src=\"./assets/svg/type_1.svg\"></ion-icon> Parqueadero</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col>\n                    <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                      <ion-radio slot=\"start\" [value]=\"2\"></ion-radio>\n                      <ion-label><ion-icon src=\"./assets/svg/type_2.svg\"></ion-icon> Espacio doméstico</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col>\n                    <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                      <ion-radio slot=\"start\" [value]=\"3\"></ion-radio>\n                      <ion-label><ion-icon src=\"./assets/svg/type_3.svg\"></ion-icon> Espacio comercial</ion-label>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col>\n                    <ion-item no-margin no-padding lines=\"none\" color=\"noColor\">\n                      <ion-radio slot=\"start\" [value]=\"4\"></ion-radio>\n                      <ion-label><ion-icon src=\"./assets/svg/type_4.svg\"></ion-icon> Equipaje</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n            </ion-radio-group>\n            <hr />\n            <input id=\"picture\" name=\"picture\" #picture type=\"file\" accept=\"image/*\" (change)=\"upload($event)\" hidden>\n            <ion-row padding-bottom justify-content-between align-items-center>\n              <ion-col no-padding size=\"auto\">\n                <ion-text text-left color=\"lightDark\">\n                  <p no-margin><small><strong>Fotos</strong></small></p>\n                </ion-text>\n              </ion-col>\n              <ion-col [hidden]=\"photos.length == 0\" no-padding size=\"auto\">\n                <ion-button (click)=\"picture.click()\" color=\"danger\">Cargar fotos</ion-button>\n              </ion-col>\n            </ion-row>\n            <ion-row id=\"empty_photos\" [hidden]=\"photos.length > 0\" padding justify-content-center align-items-center>\n              <ion-col size=\"auto\">\n                <ion-button (click)=\"picture.click()\" color=\"danger\">Cargar fotos</ion-button>\n              </ion-col>\n            </ion-row>\n            <ion-row id=\"photos\" [hidden]=\"photos.length == 0\" padding-vertical align-items-stretch>\n              <ion-col *ngFor=\"let photo of photos; let i = index;\" size=\"4\">\n                <ion-badge *ngIf=\"i == 0\" color=\"light\">Portada</ion-badge>\n                <ion-badge (click)=\"remove(photo)\" color=\"danger\">-</ion-badge>\n                <div class=\"image\" [style.backgroundImage]=\"'url(' + (photo.url || './assets/img/default_2.png') + ')'\">\n                </div>\n              </ion-col>\n            </ion-row>\n            <br />\n            <br />\n            <br />\n            <ion-row justify-content-end align-items-center>\n              <ion-col size=\"auto\" no-padding>\n                <ion-button fill=\"clear\" type=\"button\" color=\"lightDark\">Cancelar</ion-button>\n              </ion-col>\n              <ion-col size=\"auto\" no-padding>\n                <ion-button [disabled]=\"editForm.form.invalid\" type=\"submit\" color=\"danger\">Guardar\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <hr />\n      <ion-grid fixed>\n        <ion-row align-items-stretch>\n          <ion-col no-padding padding-top size=\"12\" size-md=\"8\">\n            <ion-card>\n              <ion-card-header>\n                <ion-item lines=\"none\">\n                  <ion-label no-margin>\n                    <ion-text color=\"dark\">\n                      <h2 no-margin><strong>Ubicación</strong></h2>\n                    </ion-text>\n                    <ion-text color=\"medium_2\">\n                      <p no-margin><small>{{place.city?.name}}</small></p>\n                    </ion-text>\n                  </ion-label>\n                  <ion-icon slot=\"end\" color=\"dark\" (click)=\"show.location = !show.location;\"\n                    [src]=\"(show.location)?'./assets/svg/up.svg':'./assets/svg/down.svg'\"></ion-icon>\n                </ion-item>\n              </ion-card-header>\n              <ion-card-content [hidden]=\"!show.location\">\n                <ion-row padding-bottom>\n                  <ion-col no-padding size=\"12\" size-md>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Ciudad</ion-label>\n                      <ion-select interface=\"popover\" [(ngModel)]=\"place.city_id\" name=\"city_id\"\n                        placeholder=\"Seleccione una ciudad\" required>\n                        <ion-select-option *ngFor=\"let city of cities\" [value]=\"city.id\">{{city.name}}\n                        </ion-select-option>\n                      </ion-select>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col padding-horizontal padding-top size=\"auto\"></ion-col>\n                  <ion-col no-padding size=\"12\" size-md>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Dirección</ion-label>\n                      <ion-input [(ngModel)]=\"place.address\" name=\"address\" placeholder=\"Aqui tu dirección\" required>\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n                <ion-row padding-bottom>\n                  <ion-col no-padding size=\"12\" size-md>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Cerca a</ion-label>\n                      <ion-input [(ngModel)]=\"place.reference\" name=\"reference\" placeholder=\"Ej: cerca a unicentro\">\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col class=\"ion-hide-md-down\" padding size=\"auto\"></ion-col>\n                  <ion-col class=\"ion-hide-md-down\" no-padding>\n                  </ion-col>\n                </ion-row>\n              </ion-card-content>\n            </ion-card>\n            <ion-card>\n              <ion-card-header>\n                <ion-item lines=\"none\">\n                  <ion-label no-margin>\n                    <ion-text color=\"dark\">\n                      <h2 no-margin><strong>Dimensiones</strong></h2>\n                    </ion-text>\n                    <ion-text color=\"medium_2\">\n                      <p no-margin><small>{{place.dimension}}</small></p>\n                    </ion-text>\n                  </ion-label>\n                  <ion-icon slot=\"end\" color=\"dark\" (click)=\"show.dimension = !show.dimension;\"\n                    [src]=\"(show.dimension)?'./assets/svg/up.svg':'./assets/svg/down.svg'\"></ion-icon>\n                </ion-item>\n              </ion-card-header>\n              <ion-card-content [hidden]=\"!show.dimension\">\n                <ion-row padding-bottom>\n                  <ion-col no-padding>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Largo ({{place.measure_unit}})</ion-label>\n                      <ion-input [(ngModel)]=\"place.measure_x\" name=\"measure_x\" placeholder=\"Aqui el largo\"\n                        type=\"number\" required>\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col padding size=\"auto\"></ion-col>\n                  <ion-col no-padding>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Ancho ({{place.measure_unit}})</ion-label>\n                      <ion-input [(ngModel)]=\"place.measure_y\" name=\"measure_y\" placeholder=\"Aqui el ancho\"\n                        type=\"number\" required>\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col padding size=\"auto\"></ion-col>\n                  <ion-col no-padding>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Alto ({{place.measure_unit}})</ion-label>\n                      <ion-input [(ngModel)]=\"place.measure_z\" name=\"measure_z\" placeholder=\"Aqui el alto\" type=\"number\"\n                        required>\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card-content>\n            </ion-card>\n            <ion-card>\n              <ion-card-header>\n                <ion-item lines=\"none\">\n                  <ion-label no-margin>\n                    <ion-text color=\"dark\">\n                      <h2 no-margin><strong>Horario</strong></h2>\n                    </ion-text>\n                    <ion-text color=\"medium_2\">\n                      <p no-margin><small>{{place.visit_schedule}}</small></p>\n                    </ion-text>\n                  </ion-label>\n                  <ion-icon slot=\"end\" color=\"dark\" (click)=\"show.schedule = !show.schedule;\"\n                    [src]=\"(show.schedule)?'./assets/svg/up.svg':'./assets/svg/down.svg'\"></ion-icon>\n                </ion-item>\n              </ion-card-header>\n              <ion-card-content padding-bottom [hidden]=\"!show.schedule\">\n                <ion-row padding-bottom>\n                  <ion-col no-padding>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">De entrada</ion-label>\n                      <ion-datetime [disabled]=\"has_visit_schedule\" name=\"in_at\" pickerFormat=\"hh mm a\"\n                        displayFormat=\"hh:mm a\" [(ngModel)]=\"in_at\" placeholder=\"Define un hora\" doneText=\"Aceptar\"\n                        cancelText=\"Cancelar\">\n                      </ion-datetime>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col padding size=\"auto\"></ion-col>\n                  <ion-col no-padding>\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"medium_2\">Ancho</ion-label>\n                      <ion-datetime [disabled]=\"has_visit_schedule\" name=\"out_at\" pickerFormat=\"hh mm a\"\n                        displayFormat=\"hh:mm a\" [(ngModel)]=\"out_at\" placeholder=\"Define una hora\" doneText=\"Aceptar\"\n                        cancelText=\"Cancelar\">\n                      </ion-datetime>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col no-padding *ngFor=\"let day of days\" size=\"6\" size-md=\"4\">\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-checkbox [disabled]=\"has_visit_schedule\" [name]=\"day.name\" slot=\"start\"\n                        [(ngModel)]=\"day.isChecked\">\n                      </ion-checkbox>\n                      <ion-label color=\"medium_2\">{{day.name}}</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n                <ion-item id=\"has_visit_schedule\" color=\"medium\" lines=\"none\">\n                  <ion-checkbox slot=\"start\" name=\"has_visit_schedule\" [(ngModel)]=\"has_visit_schedule\"></ion-checkbox>\n                  <ion-label color=\"lightDark\">No determinar horario de entrada ni de salida</ion-label>\n                </ion-item>\n              </ion-card-content>\n            </ion-card>\n            <ion-card>\n              <ion-card-header>\n                <ion-item lines=\"none\">\n                  <ion-label no-margin>\n                    <ion-text color=\"dark\">\n                      <h2 no-margin><strong>Carateristicas Servicios</strong></h2>\n                    </ion-text>\n                    <ion-text color=\"medium_2\">\n                      <p no-margin><small>{{getServices()}}</small></p>\n                    </ion-text>\n                  </ion-label>\n                  <ion-icon slot=\"end\" color=\"dark\" (click)=\"show.services = !show.services;\"\n                    [src]=\"(show.services)?'./assets/svg/up.svg':'./assets/svg/down.svg'\"></ion-icon>\n                </ion-item>\n              </ion-card-header>\n              <ion-card-content [hidden]=\"!show.services\">\n                <ion-row padding-bottom>\n                  <ion-col no-padding *ngFor=\"let service of services\" size=\"6\" size-md=\"4\">\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-checkbox slot=\"start\" [name]=\"'service_' + service.id\" [(ngModel)]=\"service.isChecked\">\n                      </ion-checkbox>\n                      <ion-label color=\"medium_2\">{{service.name}}</ion-label>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card-content>\n            </ion-card>\n            <ion-card>\n              <ion-card-header>\n                <ion-item lines=\"none\">\n                  <ion-label no-margin>\n                    <ion-text color=\"dark\">\n                      <h2 no-margin><strong>Descripción</strong></h2>\n                    </ion-text>\n                    <ion-text color=\"medium_2\">\n                      <p no-margin><small>Información detallada sobre tu espacio</small></p>\n                    </ion-text>\n                  </ion-label>\n                  <ion-icon slot=\"end\" color=\"dark\" (click)=\"show.description = !show.description;\"\n                    [src]=\"(show.description)?'./assets/svg/up.svg':'./assets/svg/down.svg'\"></ion-icon>\n                </ion-item>\n              </ion-card-header>\n              <ion-card-content [hidden]=\"!show.description\">\n                <ion-row padding-bottom>\n                  <ion-col no-padding size=\"12\">\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"lightDark\"><strong>Ubicación interna</strong>\n                      </ion-label>\n                      <ion-input [(ngModel)]=\"place.interal_ubication\" name=\"interal_ubication\"\n                        placeholder=\"Ej: cubiculo, bodega ...\">\n                      </ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n                <ion-row padding-bottom>\n                  <ion-col no-padding size=\"12\">\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"lightDark\"><strong>Texto destacado</strong>\n                      </ion-label>\n                      <ion-textarea rows=\"2\" [(ngModel)]=\"place.observation\" name=\"observation\"\n                        placeholder=\"Comparte en 2 lineas lo que es tu espacio ...\" required>\n                      </ion-textarea>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n                <ion-row padding-bottom>\n                  <ion-col no-padding size=\"12\">\n                    <ion-item lines=\"none\" color=\"noColor\">\n                      <ion-label position=\"stacked\" no-margin color=\"lightDark\"><strong>Más información</strong>\n                      </ion-label>\n                      <ion-textarea rows=\"4\" [(ngModel)]=\"place.description\" name=\"description\"\n                        placeholder=\"Comparte más información de tu espacio\" required>\n                      </ion-textarea>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/edit-place/edit-place.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/edit-place/edit-place.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .bg {\n  background: var(--ion-color-input); }\n\n:host hr {\n  background: var(--ion-color-icon);\n  margin-bottom: 20px;\n  margin-top: 20px;\n  height: 5px; }\n\n:host ion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px; }\n\n:host #empty_photos {\n  min-height: 100px;\n  background: var(--ion-color-input);\n  border: 10px solid var(--ion-color-border);\n  border-radius: 12px; }\n\n:host ion-card {\n  --background: var(--ion-color-light);\n  margin: 0;\n  width: 100%;\n  border-radius: 12px;\n  margin-bottom: 16px; }\n\n:host ion-card h2,\n  :host ion-card p {\n    margin: 0; }\n\n:host ion-card ion-card-header {\n    padding-bottom: 8px; }\n\n:host #photos ion-col {\n  position: relative; }\n\n:host #photos ion-col ion-badge[color=\"danger\"] {\n    position: absolute;\n    top: 10px;\n    right: 10px;\n    font-size: 20px;\n    --padding-top: 0px;\n    --padding-bottom: 0px; }\n\n:host #photos ion-col ion-badge[color=\"light\"] {\n    position: absolute;\n    top: 10px;\n    left: 10px;\n    --padding-top: 0px;\n    --padding-bottom: 0px; }\n\n:host #photos ion-col div.image {\n    width: 100%;\n    height: 120px;\n    display: block;\n    border-radius: 6px;\n    background-size: cover;\n    background-repeat: no-repeat;\n    background-position: center; }\n\n:host #has_visit_schedule {\n  --min-height: 30px;\n  --border-radius: 6px;\n  border-radius: 6px; }\n\n:host #has_visit_schedule ion-checkbox {\n    margin: 0px 10px; }\n\n:host #has_visit_schedule ion-label {\n    --padding-top: 0px;\n    --padding-bottom: 0px;\n    margin: 0; }\n\n:host ion-radio {\n  --border-radius: 6px;\n  --color-checked: var(--ion-color-danger)\n    ; }\n\n:host ion-checkbox {\n  --border-radius: 6px; }\n\n:host ion-select {\n  background: var(--ion-color-light);\n  margin-top: 8px;\n  border: 1px solid var(--ion-color-border, #AAAAAA);\n  --border-radius: 8px;\n  border-radius: 8px;\n  --padding-start: 10px;\n  --padding-end: 10px; }\n\n:host ion-input {\n  background: var(--ion-color-light);\n  margin-top: 8px;\n  border: 1px solid var(--ion-color-border, #AAAAAA);\n  --border-radius: 8px;\n  border-radius: 8px;\n  --padding-start: 10px;\n  --padding-end: 10px; }\n\n:host ion-datetime {\n  background: var(--ion-color-light);\n  margin-top: 8px;\n  border: 1px solid var(--ion-color-border, #AAAAAA);\n  --border-radius: 8px;\n  border-radius: 8px;\n  --padding-start: 10px;\n  --padding-end: 10px;\n  --padding-top: 8px;\n  --padding-bottom: 8px; }\n\n:host ion-textarea {\n  background: var(--ion-color-light);\n  margin-top: 8px;\n  border: 1px solid var(--ion-color-border, #AAAAAA);\n  --border-radius: 8px;\n  border-radius: 8px;\n  --padding-start: 10px;\n  --padding-end: 10px;\n  --padding-top: 8px;\n  --padding-bottom: 8px; }\n\n:host ion-item#place {\n  border: 1px solid #F0F1F3;\n  border-radius: 8px;\n  --border-radius: 8px;\n  margin-left: 5px; }\n\n:host div.image {\n  display: block;\n  width: 100%;\n  height: 100%;\n  margin-right: 5px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvZWRpdC1wbGFjZS9lZGl0LXBsYWNlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGtDQUFrQyxFQUFBOztBQUYxQztFQU1RLGlDQUFpQztFQUNqQyxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFdBQVcsRUFBQTs7QUFUbkI7RUFhUSxvQkFBZ0I7RUFDaEIsd0JBQW9CLEVBQUE7O0FBZDVCO0VBa0JRLGlCQUFpQjtFQUNqQixrQ0FBa0M7RUFDbEMsMENBQTBDO0VBQzFDLG1CQUFtQixFQUFBOztBQXJCM0I7RUF5QlEsb0NBQWE7RUFDYixTQUFTO0VBQ1QsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixtQkFBbUIsRUFBQTs7QUE3QjNCOztJQWlDWSxTQUFTLEVBQUE7O0FBakNyQjtJQXFDWSxtQkFBbUIsRUFBQTs7QUFyQy9CO0VBMkNZLGtCQUFrQixFQUFBOztBQTNDOUI7SUE4Q2dCLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsV0FBVztJQUNYLGVBQWU7SUFDZixrQkFBYztJQUNkLHFCQUFpQixFQUFBOztBQW5EakM7SUF1RGdCLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsVUFBVTtJQUNWLGtCQUFjO0lBQ2QscUJBQWlCLEVBQUE7O0FBM0RqQztJQStEZ0IsV0FBVztJQUNYLGFBQWE7SUFDYixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0Qiw0QkFBNEI7SUFDNUIsMkJBQTJCLEVBQUE7O0FBckUzQztFQTJFUSxrQkFBYTtFQUNiLG9CQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUE3RTFCO0lBZ0ZZLGdCQUFnQixFQUFBOztBQWhGNUI7SUFvRlksa0JBQWM7SUFDZCxxQkFBaUI7SUFDakIsU0FBUyxFQUFBOztBQXRGckI7RUEyRlEsb0JBQWdCO0VBQ2hCO0lBQWdCLEVBQUE7O0FBNUZ4QjtFQWdHUSxvQkFBZ0IsRUFBQTs7QUFoR3hCO0VBb0dRLGtDQUFrQztFQUNsQyxlQUFlO0VBQ2Ysa0RBQWtEO0VBQ2xELG9CQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIscUJBQWdCO0VBQ2hCLG1CQUFjLEVBQUE7O0FBMUd0QjtFQThHUSxrQ0FBa0M7RUFDbEMsZUFBZTtFQUNmLGtEQUFrRDtFQUNsRCxvQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHFCQUFnQjtFQUNoQixtQkFBYyxFQUFBOztBQXBIdEI7RUF3SFEsa0NBQWtDO0VBQ2xDLGVBQWU7RUFDZixrREFBa0Q7RUFDbEQsb0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixxQkFBZ0I7RUFDaEIsbUJBQWM7RUFDZCxrQkFBYztFQUNkLHFCQUFpQixFQUFBOztBQWhJekI7RUFvSVEsa0NBQWtDO0VBQ2xDLGVBQWU7RUFDZixrREFBa0Q7RUFDbEQsb0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixxQkFBZ0I7RUFDaEIsbUJBQWM7RUFDZCxrQkFBYztFQUNkLHFCQUFpQixFQUFBOztBQTVJekI7RUFpSlkseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixvQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBcEo1QjtFQXlKUSxjQUFjO0VBQ2QsV0FBVztFQUNYLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLDRCQUE0QjtFQUM1QiwyQkFBMkIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VkaXQtcGxhY2UvZWRpdC1wbGFjZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgLmJnIHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWlucHV0KTtcbiAgICB9XG5cbiAgICBociB7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pY29uKTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgaGVpZ2h0OiA1cHg7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAgIH1cblxuICAgICNlbXB0eV9waG90b3Mge1xuICAgICAgICBtaW4taGVpZ2h0OiAxMDBweDtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWlucHV0KTtcbiAgICAgICAgYm9yZGVyOiAxMHB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1ib3JkZXIpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgIH1cblxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuXG4gICAgICAgIGgyLFxuICAgICAgICBwIHtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgI3Bob3RvcyB7XG4gICAgICAgIGlvbi1jb2wge1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICBpb24tYmFkZ2VbY29sb3I9XCJkYW5nZXJcIl0ge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICB0b3A6IDEwcHg7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgIC0tcGFkZGluZy10b3A6IDBweDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1iYWRnZVtjb2xvcj1cImxpZ2h0XCJdIHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgICAgLS1wYWRkaW5nLXRvcDogMHB4O1xuICAgICAgICAgICAgICAgIC0tcGFkZGluZy1ib3R0b206IDBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZGl2LmltYWdlIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEyMHB4O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgI2hhc192aXNpdF9zY2hlZHVsZSB7XG4gICAgICAgIC0tbWluLWhlaWdodDogMzBweDtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcblxuICAgICAgICBpb24tY2hlY2tib3gge1xuICAgICAgICAgICAgbWFyZ2luOiAwcHggMTBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAtLXBhZGRpbmctdG9wOiAwcHg7XG4gICAgICAgICAgICAtLXBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpb24tcmFkaW8ge1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgLS1jb2xvci1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKVxuICAgIH1cblxuICAgIGlvbi1jaGVja2JveCB7XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIH1cblxuICAgIGlvbi1zZWxlY3Qge1xuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1ib3JkZXIsICNBQUFBQUEpO1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gICAgICAgIC0tcGFkZGluZy1lbmQ6IDEwcHg7XG4gICAgfVxuXG4gICAgaW9uLWlucHV0IHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItYm9yZGVyLCAjQUFBQUFBKTtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAxMHB4O1xuICAgIH1cblxuICAgIGlvbi1kYXRldGltZSB7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgICAgIG1hcmdpbi10b3A6IDhweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLWJvcmRlciwgI0FBQUFBQSk7XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLXRvcDogOHB4O1xuICAgICAgICAtLXBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgfVxuXG4gICAgaW9uLXRleHRhcmVhIHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItYm9yZGVyLCAjQUFBQUFBKTtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAxMHB4O1xuICAgICAgICAtLXBhZGRpbmctdG9wOiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1ib3R0b206IDhweDtcbiAgICB9XG5cbiAgICBpb24taXRlbSB7XG4gICAgICAgICYjcGxhY2Uge1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0YwRjFGMztcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRpdi5pbWFnZSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/edit-place/edit-place.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/edit-place/edit-place.page.ts ***!
  \*****************************************************/
/*! exports provided: EditPlacePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPlacePage", function() { return EditPlacePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! async */ "./node_modules/async/dist/async.js");
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(async__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment/moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment_moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var EditPlacePage = /** @class */ (function () {
    function EditPlacePage(route, navCtrl, toastCtrl, generalService, placeService) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.generalService = generalService;
        this.placeService = placeService;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
        this.place = new _models__WEBPACK_IMPORTED_MODULE_6__["Place"]();
        this.photos = [];
        this.files = [];
        this.days = [{ name: "Lunes", isChecked: false }, { name: "Martes", isChecked: false }, { name: "Miercoles", isChecked: false }, { name: "Jueves", isChecked: false }, { name: "Viernes", isChecked: false }, { name: "Sabado", isChecked: false }, { name: "Domingo", isChecked: false }];
        this.show = {
            location: false,
            dimension: false,
            schedule: false,
            services: true,
            description: false
        };
    }
    EditPlacePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.place_id = Number(params.get("place_id"));
            if (!!_this.place_id)
                _this.get();
        });
        this.generalService.cities().subscribe(function (response) {
            _this.cities = response;
        });
    };
    EditPlacePage.prototype.upload = function (event) {
        var _this = this;
        if (event.target.files && event.target.files.length > 0) {
            this.files = this.files.concat(event.target.files);
            async__WEBPACK_IMPORTED_MODULE_3___default.a.each(event.target.files, function (file, callback) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    _this.photos.push({ id: null, url: e.target['result'] });
                    callback();
                };
                reader.readAsDataURL(file);
            }, function (err) { });
        }
    };
    EditPlacePage.prototype.remove = function (photo) {
        if (!photo.id) {
            var index_1 = this.photos.filter(function (photo) { return !photo.id; }).indexOf(photo);
            this.files.splice(index_1, 1);
        }
        var index = this.photos.indexOf(photo);
        this.photos.splice(index, 1);
    };
    EditPlacePage.prototype.send = function () {
        var _this = this;
        var days = this.days.filter(function (day) { return day.isChecked; }).map(function (day) { return day.name.substr(0, 3); });
        if (days.length == 7) {
            this.place.visit_schedule = "Todo los dias";
        }
        else {
            this.place.visit_schedule = days.join(", ");
        }
        if (this.in_at && this.out_at) {
            this.place.visit_schedule += " de " + moment_moment__WEBPACK_IMPORTED_MODULE_4__(this.in_at, "HH:mm").format("h:mm a") + " - " + moment_moment__WEBPACK_IMPORTED_MODULE_4__(this.out_at, "HH:mm").format("h:mm a");
        }
        else if (this.in_at) {
            this.place.visit_schedule += " desde " + moment_moment__WEBPACK_IMPORTED_MODULE_4__(this.in_at, "HH:mm").format("h:mm a");
        }
        else if (this.out_at) {
            this.place.visit_schedule += " hasta " + moment_moment__WEBPACK_IMPORTED_MODULE_4__(this.out_at, "HH:mm").format("h:mm a");
        }
        async__WEBPACK_IMPORTED_MODULE_3___default.a.each(this.files, function (file, callback) {
            if (file) {
                _this.generalService.upload(file).subscribe(function (response) {
                    _this.place.photos.push(response);
                    callback();
                }, callback);
            }
            else {
                callback();
            }
        }, function (err) {
            if (err)
                _this.showToast(err);
            else
                _this.setPhotos();
        });
    };
    EditPlacePage.prototype.setPhotos = function () {
        var _this = this;
        var _remove = this.place.photos.filter(function (photo) { return !_this.photos.find(function (s) { return s.id == photo.id; }); });
        async__WEBPACK_IMPORTED_MODULE_3___default.a.each(_remove, function (photo, callback) {
            _this.placeService.removePhoto(_this.place.id, photo.id).subscribe(function () { return callback(); }, function () { return callback(); });
        }, function () {
            _this.setServices();
        });
    };
    EditPlacePage.prototype.setServices = function () {
        var _this = this;
        var _add = this.services.filter(function (service) { return service.isChecked; }).filter(function (service) { return !_this.place.services.find(function (s) { return s.id == service.id; }); });
        var _remove = this.services.filter(function (service) { return !service.isChecked; }).filter(function (service) { return _this.place.services.find(function (s) { return s.id == service.id; }); });
        async__WEBPACK_IMPORTED_MODULE_3___default.a.each(_add, function (service, callback) {
            _this.placeService.addService(_this.place.id, service).subscribe(function () { return callback(); }, function () { return callback(); });
        }, function () {
            async__WEBPACK_IMPORTED_MODULE_3___default.a.each(_remove, function (service, callback) {
                _this.placeService.removeService(_this.place.id, service.id).subscribe(function () { return callback(); }, function () { return callback(); });
            }, function () {
                _this.request();
            });
        });
    };
    EditPlacePage.prototype.request = function () {
        var _this = this;
        this.placeService.edit(this.place).subscribe(function (response) {
            _this.showToast("Tu anuncio ha sido editado.");
            _this.navCtrl.back();
        });
    };
    EditPlacePage.prototype.listServices = function () {
        var _this = this;
        this.services = [];
        this.generalService.services({ limit: -1, filters: "type_id:" + this.place.type_id }).subscribe(function (response) {
            _this.services = response;
            _this.services.forEach(function (service) {
                service.isChecked = _this.place.services.find(function (s) { return s.id == service.id; }) != null;
            });
        });
    };
    EditPlacePage.prototype.getServices = function () {
        return this.place.services.map(function (service) { return service.name; }).join("; ");
    };
    EditPlacePage.prototype.get = function () {
        var _this = this;
        this.placeService.get(this.place_id, { latitude: null, longitude: null, from: null, to: null }).subscribe(function (response) {
            _this.place = response;
            _this.photos = _this.place.photos.slice();
            if (!_this.place.visit_schedule || _this.place.visit_schedule.trim() == "") {
                _this.has_visit_schedule = true;
            }
            else {
                var days = "";
                if (_this.place.visit_schedule.includes(" desde ")) {
                    var temp = _this.place.visit_schedule.split(" desde ");
                    days = temp[0];
                    _this.in_at = moment_moment__WEBPACK_IMPORTED_MODULE_4__(temp[1], "h:mm a").format();
                }
                else if (_this.place.visit_schedule.includes(" de ")) {
                    var temp = _this.place.visit_schedule.split(" de ");
                    days = temp[0];
                    temp = temp[1].split(" - ");
                    _this.in_at = moment_moment__WEBPACK_IMPORTED_MODULE_4__(temp[0], "h:mm a").format();
                    _this.out_at = moment_moment__WEBPACK_IMPORTED_MODULE_4__(temp[1], "h:mm a").format();
                }
                else if (_this.place.visit_schedule.includes(" hasta ")) {
                    var temp = _this.place.visit_schedule.split(" hasta ");
                    days = temp[0];
                    _this.out_at = moment_moment__WEBPACK_IMPORTED_MODULE_4__(temp[1], "h:mm a").format();
                }
                else {
                    days = _this.place.visit_schedule;
                }
                if (!!days) {
                    if (days == "Todo los dias") {
                        _this.days.forEach(function (day) { day.isChecked = true; });
                    }
                    else {
                        days.split(", ").forEach(function (day) {
                            (_this.days.find(function (d) { return d.name.substr(0, 3) == day; }) || { isChecked: false }).isChecked = true;
                        });
                    }
                }
            }
            _this.listServices();
        });
    };
    EditPlacePage.prototype.showToast = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 2000,
                            showCloseButton: true,
                            position: 'bottom',
                            closeButtonText: 'Aceptar'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('picture'),
        __metadata("design:type", Object)
    ], EditPlacePage.prototype, "picture", void 0);
    EditPlacePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-place',
            template: __webpack_require__(/*! ./edit-place.page.html */ "./src/app/pages/edit-place/edit-place.page.html"),
            styles: [__webpack_require__(/*! ./edit-place.page.scss */ "./src/app/pages/edit-place/edit-place.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"], _services__WEBPACK_IMPORTED_MODULE_5__["GeneralService"], _services__WEBPACK_IMPORTED_MODULE_5__["PlaceService"]])
    ], EditPlacePage);
    return EditPlacePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-edit-place-edit-place-module.js.map