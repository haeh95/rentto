(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-places-my-places-module"],{

/***/ "./src/app/pages/my-places/my-places.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-places/my-places.module.ts ***!
  \*****************************************************/
/*! exports provided: MyPlacesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyPlacesPageModule", function() { return MyPlacesPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _my_places_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-places.page */ "./src/app/pages/my-places/my-places.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _my_places_page__WEBPACK_IMPORTED_MODULE_5__["MyPlacesPage"]
    }
];
var MyPlacesPageModule = /** @class */ (function () {
    function MyPlacesPageModule() {
    }
    MyPlacesPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_my_places_page__WEBPACK_IMPORTED_MODULE_5__["MyPlacesPage"]]
        })
    ], MyPlacesPageModule);
    return MyPlacesPageModule;
}());



/***/ }),

/***/ "./src/app/pages/my-places/my-places.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-places/my-places.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-toolbar class=\"ion-hide-md-up\" padding-top>\n  <ion-buttons slot=\"start\">\n    <ion-button (click)=\"goBack()\" fill=\"clear\" color=\"medium_2\">\n      <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n    </ion-button>\n    <ion-label>\n      <ion-text color=\"medium\">\n        <h1 no-margin>Mis</h1>\n      </ion-text>\n      <ion-text color=\"dark\">\n        <h1 no-margin><strong>Publicaciones</strong></h1>\n      </ion-text>\n    </ion-label>\n  </ion-buttons>\n</ion-toolbar>\n<ion-content padding>\n  <ion-row padding>\n    <ion-col *ngFor=\"let place of places\" size=\"12\" size-md=\"4\">\n      <app-my-place (refresh)=\"list()\" [place]=\"place\"></app-my-place>\n    </ion-col>\n  </ion-row>\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas lugares...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/my-places/my-places.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-places/my-places.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-toolbar {\n  padding-left: 8px;\n  padding-right: 8px;\n  --padding-left: 8px;\n  --padding-right: 8px; }\n\n:host ion-content {\n  --padding-bottom: 80px; }\n\n@media (max-width: 820px) {\n    :host ion-content ion-row {\n      padding-left: 0;\n      padding-right: 0;\n      --padding-left: 0;\n      --padding-right: 0; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvbXktcGxhY2VzL215LXBsYWNlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG1CQUFlO0VBQ2Ysb0JBQWdCLEVBQUE7O0FBTHhCO0VBU1Esc0JBQWlCLEVBQUE7O0FBRWpCO0lBWFI7TUFhZ0IsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixpQkFBZTtNQUNmLGtCQUFnQixFQUFBLEVBQ25CIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbXktcGxhY2VzL215LXBsYWNlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW9uLXRvb2xiYXIge1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogOHB4O1xuICAgICAgICAtLXBhZGRpbmctbGVmdDogOHB4O1xuICAgICAgICAtLXBhZGRpbmctcmlnaHQ6IDhweDtcbiAgICB9XG5cbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tcGFkZGluZy1ib3R0b206IDgwcHg7XG5cbiAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gICAgICAgICAgICBpb24tcm93IHtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctbGVmdDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctcmlnaHQ6IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/my-places/my-places.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/my-places/my-places.page.ts ***!
  \***************************************************/
/*! exports provided: MyPlacesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyPlacesPage", function() { return MyPlacesPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyPlacesPage = /** @class */ (function () {
    function MyPlacesPage(navCtrl, placeService) {
        this.navCtrl = navCtrl;
        this.placeService = placeService;
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_1__["Pagination"]();
    }
    MyPlacesPage.prototype.ngOnInit = function () {
        this.list();
    };
    MyPlacesPage.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.places = response.models;
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    MyPlacesPage.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.places = _this.places.concat(response.models);
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    MyPlacesPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    MyPlacesPage.prototype.load = function (callback) {
        this.placeService.myPlaces({
            page: this.pagination.page,
            limit: 12,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
    ], MyPlacesPage.prototype, "infiniteScroll", void 0);
    MyPlacesPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-my-places',
            template: __webpack_require__(/*! ./my-places.page.html */ "./src/app/pages/my-places/my-places.page.html"),
            styles: [__webpack_require__(/*! ./my-places.page.scss */ "./src/app/pages/my-places/my-places.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_app_services__WEBPACK_IMPORTED_MODULE_3__["PlaceService"]])
    ], MyPlacesPage);
    return MyPlacesPage;
}());



/***/ })

}]);
//# sourceMappingURL=my-places-my-places-module.js.map