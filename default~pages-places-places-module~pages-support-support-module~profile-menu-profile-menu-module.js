(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-places-places-module~pages-support-support-module~profile-menu-profile-menu-module"],{

/***/ "./src/app/modals/chat-support/chat-support.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/modals/chat-support/chat-support.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-toolbar padding color=\"input\">\n  <ion-text color=\"dark\">\n    <h1 no-margin><strong>Chat con soporte</strong></h1>\n  </ion-text>\n  <ion-text color=\"medium\">\n    <p no-margin>En instantes te atendera uno de nuestros asesores</p>\n  </ion-text>\n  <ion-buttons slot=\"end\">\n    <ion-button (click)=\"close()\" fill=\"clear\" color=\"medium_3\">\n      <ion-icon slot=\"icon-only\" src=\"./assets/svg/close.svg\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n</ion-toolbar>\n<ion-content padding>\n  <ion-infinite-scroll position=\"top\" threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas chats...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <ion-row nowrap *ngFor=\"let message of messages\">\n    <ion-col [offset]=\"(message.from_id == user.id)?7:0\" size=\"5\">\n      <p class=\"message\" [ngClass]=\"{ 'me': message.from_id == user.id }\" no-margin>{{message.message}}</p>\n      <p no-margin>{{message.created_at | date}}</p>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <form #messageForm=\"ngForm\" (ngSubmit)=\"send()\" novalidate>\n    <ion-item lines=\"none\" color=\"messageBar\">\n      <ion-input [(ngModel)]=\"text\" name=\"text\" (keyup.enter)=\"messageForm.onSubmit()\" placeholder=\"Escribir un mensaje\"\n        required></ion-input>\n      <ion-icon slot=\"end\" src=\"./assets/svg/camera.svg\"></ion-icon>\n    </ion-item>\n  </form>\n</ion-footer>"

/***/ }),

/***/ "./src/app/modals/chat-support/chat-support.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/modals/chat-support/chat-support.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  padding-bottom: 8px;\n  background: var(--ion-color-messageBar);\n  border-radius: 8px; }\n  :host ion-content p.message {\n    border-radius: 5px;\n    padding: 8px;\n    background: var(--ion-color-messageBar); }\n  :host ion-content p.message.me {\n      background: var(--ion-color-primary);\n      color: var(--ion-color-light); }\n  :host ion-content p:not(.message) {\n    text-align: right;\n    font-size: 12px;\n    color: var(--ion-color-medium_2);\n    margin-top: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvbW9kYWxzL2NoYXQtc3VwcG9ydC9jaGF0LXN1cHBvcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUI7RUFDbkIsdUNBQXVDO0VBQ3ZDLGtCQUFrQixFQUFBO0VBSHRCO0lBVWdCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osdUNBQXVDLEVBQUE7RUFadkQ7TUFlb0Isb0NBQW9DO01BQ3BDLDZCQUE2QixFQUFBO0VBaEJqRDtJQXFCZ0IsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixnQ0FBZ0M7SUFDaEMsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxzL2NoYXQtc3VwcG9ydC9jaGF0LXN1cHBvcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWVzc2FnZUJhcik7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuXG4gICAgaW9uLWNvbnRlbnQge1xuXG4gICAgICAgIHAge1xuXG4gICAgICAgICAgICAmLm1lc3NhZ2Uge1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA4cHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1lc3NhZ2VCYXIpO1xuXG4gICAgICAgICAgICAgICAgJi5tZSB7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAmOm5vdCgubWVzc2FnZSkge1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bV8yKTtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/modals/chat-support/chat-support.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/modals/chat-support/chat-support.component.ts ***!
  \***************************************************************/
/*! exports provided: ChatSupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatSupportComponent", function() { return ChatSupportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatSupportComponent = /** @class */ (function () {
    function ChatSupportComponent(chatService, viewCtrl) {
        this.chatService = chatService;
        this.viewCtrl = viewCtrl;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_4__["Pagination"]();
    }
    ChatSupportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.chatService.requestSupport().subscribe(function (response) {
            _this.chat_id = response.id;
            _this.list();
        });
    };
    ChatSupportComponent.prototype.send = function () {
        var _this = this;
        if (this.messageForm.form.valid)
            this.chatService.message(this.chat_id, this.text).subscribe(function (response) {
                _this.messages.push(response);
                _this.text = "";
            });
    };
    ChatSupportComponent.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.messages = response.models;
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatSupportComponent.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.messages = _this.messages.concat(response.models);
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatSupportComponent.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ChatSupportComponent.prototype.load = function (callback) {
        this.chatService.messages(this.chat_id, {
            page: this.pagination.page,
            limit: 20,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("messageForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], ChatSupportComponent.prototype, "messageForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], ChatSupportComponent.prototype, "infiniteScroll", void 0);
    ChatSupportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chat-support',
            template: __webpack_require__(/*! ./chat-support.component.html */ "./src/app/modals/chat-support/chat-support.component.html"),
            styles: [__webpack_require__(/*! ./chat-support.component.scss */ "./src/app/modals/chat-support/chat-support.component.scss")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_3__["ChatService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], ChatSupportComponent);
    return ChatSupportComponent;
}());



/***/ }),

/***/ "./src/app/modals/filters/filters.component.html":
/*!*******************************************************!*\
  !*** ./src/app/modals/filters/filters.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header padding>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"back()\" color=\"medium\">\n        <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-row>\n    <ion-col>\n      <ion-text text-start color=\"dark\">\n        <h2 no-margin><strong>Selecciona que mostrar:</strong></h2>\n      </ion-text>\n      <ion-text text-start color=\"medium_2\">\n        <p no-margin>Se mostrara en el mapa y lista de acuerdo a la configuración que realices</p>\n      </ion-text>\n    </ion-col>\n  </ion-row>\n  <ion-row padding-vertical>\n    <ion-radio-group [(ngModel)]=\"filter.status\" name=\"type_id\">\n      <ion-item no-margin no-padding lines=\"none\">\n        <ion-radio slot=\"start\" [value]=\"null\"></ion-radio>\n        <ion-label>Mostrar todos</ion-label>\n      </ion-item>\n      <ion-item no-margin no-padding lines=\"none\">\n        <ion-radio slot=\"start\" [value]=\"'available'\"></ion-radio>\n        <ion-label>Solo mostrar disponibles</ion-label>\n      </ion-item>\n      <ion-item no-margin no-padding lines=\"none\">\n        <ion-radio slot=\"start\" [value]=\"'not-available'\"></ion-radio>\n        <ion-label>Solo mostrar no disponibles</ion-label>\n      </ion-item>\n    </ion-radio-group>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-text text-start color=\"dark\">\n        <h2 no-margin><strong>Rango de precios:</strong></h2>\n      </ion-text>\n      <ion-text text-start color=\"medium_2\">\n        <p no-margin>Selecciona un rango de precios según tu disponibilidad</p>\n      </ion-text>\n    </ion-col>\n  </ion-row>\n  <ion-row justify-content-between>\n    <ion-col size=\"auto\">\n      <ion-text color=\"medium\">\n        ${{filter.price | number:'1.0-0'}}/{{(filter.type == 4)?'hora':'mes'}}\n      </ion-text>\n    </ion-col>\n    <ion-col size=\"auto\">\n      <ion-text color=\"medium\">\n        Cualquiera\n      </ion-text>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-range no-padding padding-horizontal color=\"primary\" [max]=\"max_price\" step=\"1000\" [(ngModel)]=\"filter.price\"\n        name=\"price\" pin=\"true\">\n      </ion-range>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-text text-start color=\"dark\">\n        <h2 no-margin><strong>Caracteristicas:</strong></h2>\n      </ion-text>\n      <ion-text text-start color=\"medium_2\">\n        <p no-margin>Selecciona las caracteristicas que desea</p>\n      </ion-text>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\" no-padding *ngFor=\"let service of services\">\n      <ion-item no-padding no-margin lines=\"none\">\n        <ion-checkbox slot=\"start\" [name]=\"'service_id' + service.id\" [(ngModel)]=\"service.isChecked\"></ion-checkbox>\n        <ion-label color=\"dark\"><ion-icon [src]=\"'./assets/categories/' + filter.type + '/' + service.id + '.svg'\"></ion-icon>{{service.name}}</ion-label>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer no-border>\n  <ion-toolbar padding-horizontal padding-bottom>\n    <ion-button (click)=\"apply()\" size=\"large\" expand=\"block\" color=\"danger\">Aplicar</ion-button>\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./src/app/modals/filters/filters.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/modals/filters/filters.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-header {\n  --border: none;\n  border: none; }\n  :host ion-header::after {\n    display: none; }\n  :host ion-footer {\n  --border: none;\n  border: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvbW9kYWxzL2ZpbHRlcnMvZmlsdGVycy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGNBQVM7RUFDVCxZQUFZLEVBQUE7RUFIcEI7SUFNWSxhQUFhLEVBQUE7RUFOekI7RUFXUSxjQUFTO0VBQ1QsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxzL2ZpbHRlcnMvZmlsdGVycy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24taGVhZGVyIHtcbiAgICAgICAgLS1ib3JkZXI6IG5vbmU7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcblxuICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgaW9uLWZvb3RlciB7XG4gICAgICAgIC0tYm9yZGVyOiBub25lO1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/modals/filters/filters.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/modals/filters/filters.component.ts ***!
  \*****************************************************/
/*! exports provided: FiltersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersComponent", function() { return FiltersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FiltersComponent = /** @class */ (function () {
    function FiltersComponent(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    FiltersComponent.prototype.back = function () {
        this.viewCtrl.dismiss();
    };
    FiltersComponent.prototype.apply = function () {
        this.viewCtrl.dismiss(this.filter);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _models__WEBPACK_IMPORTED_MODULE_2__["QueryFilter"])
    ], FiltersComponent.prototype, "filter", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], FiltersComponent.prototype, "max_price", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], FiltersComponent.prototype, "services", void 0);
    FiltersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filters',
            template: __webpack_require__(/*! ./filters.component.html */ "./src/app/modals/filters/filters.component.html"),
            styles: [__webpack_require__(/*! ./filters.component.scss */ "./src/app/modals/filters/filters.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], FiltersComponent);
    return FiltersComponent;
}());



/***/ }),

/***/ "./src/app/modals/logout/logout.component.html":
/*!*****************************************************!*\
  !*** ./src/app/modals/logout/logout.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img src=\"./assets/img/sleepy.png\">\n<ion-row justify-content-center padding-vertical>\n  <ion-col size=\"10\">\n    <ion-text text-center color=\"danger\">\n      <h1 no-margin>¿Seguro de Cerrar Sesión?</h1>\n    </ion-text>\n    <ion-text text-center color=\"medium_2\">\n      <p no-margin>¡Esperamos que regreses pronto!</p>\n    </ion-text>\n  </ion-col>\n</ion-row>\n<ion-row>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"cancel()\" color=\"danger\">Cancelar</ion-button>\n  </ion-col>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"logout()\" color=\"input\">Cerrar sesión</ion-button>\n  </ion-col>\n</ion-row>"

/***/ }),

/***/ "./src/app/modals/logout/logout.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/modals/logout/logout.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host img {\n  margin: auto;\n  display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvbW9kYWxzL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxZQUFZO0VBQ1osY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxzL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW1nIHtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/modals/logout/logout.component.ts":
/*!***************************************************!*\
  !*** ./src/app/modals/logout/logout.component.ts ***!
  \***************************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LogoutComponent = /** @class */ (function () {
    function LogoutComponent(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    LogoutComponent.prototype.ngOnInit = function () {
    };
    LogoutComponent.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    LogoutComponent.prototype.logout = function () {
        this.viewCtrl.dismiss('logout');
    };
    LogoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logout',
            template: __webpack_require__(/*! ./logout.component.html */ "./src/app/modals/logout/logout.component.html"),
            styles: [__webpack_require__(/*! ./logout.component.scss */ "./src/app/modals/logout/logout.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ }),

/***/ "./src/app/modals/modals.module.ts":
/*!*****************************************!*\
  !*** ./src/app/modals/modals.module.ts ***!
  \*****************************************/
/*! exports provided: ModalsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalsModule", function() { return ModalsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _chat_support_chat_support_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat-support/chat-support.component */ "./src/app/modals/chat-support/chat-support.component.ts");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./logout/logout.component */ "./src/app/modals/logout/logout.component.ts");
/* harmony import */ var _filters_filters_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filters/filters.component */ "./src/app/modals/filters/filters.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var components = [_chat_support_chat_support_component__WEBPACK_IMPORTED_MODULE_6__["ChatSupportComponent"], _logout_logout_component__WEBPACK_IMPORTED_MODULE_7__["LogoutComponent"], _filters_filters_component__WEBPACK_IMPORTED_MODULE_8__["FiltersComponent"]];
var ModalsModule = /** @class */ (function () {
    function ModalsModule() {
    }
    ModalsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: components,
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__["PipesModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"]],
            exports: components,
            entryComponents: components
        })
    ], ModalsModule);
    return ModalsModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-places-places-module~pages-support-support-module~profile-menu-profile-menu-module.js.map