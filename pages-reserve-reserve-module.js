(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-reserve-reserve-module"],{

/***/ "./src/app/pages/reserve/reserve.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/reserve/reserve.module.ts ***!
  \*************************************************/
/*! exports provided: ReservePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservePageModule", function() { return ReservePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _reserve_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reserve.page */ "./src/app/pages/reserve/reserve.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _reserve_page__WEBPACK_IMPORTED_MODULE_5__["ReservePage"]
    }
];
var ReservePageModule = /** @class */ (function () {
    function ReservePageModule() {
    }
    ReservePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_reserve_page__WEBPACK_IMPORTED_MODULE_5__["ReservePage"]]
        })
    ], ReservePageModule);
    return ReservePageModule;
}());



/***/ }),

/***/ "./src/app/pages/reserve/reserve.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/reserve/reserve.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark></app-header-dark>\n<ion-content padding-top>\n  <ion-grid padding-top fixed>\n    <ion-row padding-top align-items-stretch>\n      <ion-col padding>\n        <ion-text color=\"dark\">\n          <p [style.font-size.px]=\"18\" no-margin><strong>Confirmar</strong></p>\n        </ion-text>\n        <ion-text color=\"medium_2\">\n          <p [style.font-size.px]=\"12\">Revisa y edita si tienes algun error antes de proceder a confirmar tu reserva</p>\n        </ion-text>\n        <br />\n        <ion-text color=\"lightDark\">\n          <p [style.font-size.px]=\"14\" no-margin><strong>Espacio</strong></p>\n        </ion-text>\n        <ion-text color=\"dark\">\n          <h1 no-margin><strong>{{place.name}}</strong></h1>\n        </ion-text>\n        <br />\n        <br />\n        <form #reserveForm=\"ngForm\" (ngSubmit)=\"reserve()\" novalidate>\n          <ion-row padding-vertical>\n            <ion-col size=\"12\" size-md no-padding>\n              <ion-text color=\"medium_2\">\n                <p no-margin [style.font-size.px]=\"12\" [style.margin-bottom.px]=\"5\">Desde</p>\n              </ion-text>\n              <ion-item lines=\"none\" color=\"input\">\n                <ion-datetime name=\"start_at\" [yearValues]=\"yearValues\"\n                  [monthNames]=\"monthNames\" [pickerFormat]=\"pickerFormat\" [displayFormat]=\"displayFormat\"\n                  [min]=\"start_min\" [(ngModel)]=\"filter.start_at\" placeholder=\"Fecha de inicio\" doneText=\"Aceptar\"\n                  cancelText=\"Cancelar\" required></ion-datetime>\n              </ion-item>\n            </ion-col>\n            <ion-col size=\"12\" size-md=\"auto\"></ion-col>\n            <ion-col size=\"12\" size-md no-padding>\n              <ion-text color=\"medium_2\">\n                <p no-margin [style.font-size.px]=\"12\" [style.margin-bottom.px]=\"5\">Tiempo</p>\n              </ion-text>\n              <ion-item lines=\"none\" color=\"input\">\n                <ion-input name=\"duration\" [(ngModel)]=\"filter.duration\" placeholder=\"Duración\" inputmode=\"number\"\n                  type=\"number\" required></ion-input>\n              </ion-item>\n            </ion-col>\n            <ion-col class=\"ion-hide-md-down\" size=\"2\"></ion-col>\n          </ion-row>\n        </form>\n      </ion-col>\n      <ion-col class=\"ion-hide-md-down\" no-padding size=\"4\">\n        <ion-card>\n          <ion-card-header>\n            <ion-card-title>\n              <ion-text color=\"dark\">\n                <strong>${{place.price | number:'1.0-0'}}</strong>\n              </ion-text>\n            </ion-card-title>\n            <ion-card-subtitle no-margin>\n              <ion-text color=\"dark\">\n                Por {{place.price_duration}}\n              </ion-text>\n            </ion-card-subtitle>\n          </ion-card-header>\n          <ion-card-content class=\"gradient-red\">\n            <ion-row padding-top>\n              <ion-col>\n                <ion-card-title>\n                  <ion-text color=\"light\">\n                    <strong>${{place.price * filter.duration | number:'1.0-0'}}</strong>\n                  </ion-text>\n                </ion-card-title>\n                <ion-card-subtitle no-margin>\n                  <ion-text color=\"light\">\n                    Total\n                  </ion-text>\n                </ion-card-subtitle>\n              </ion-col>\n              <ion-col size=\"auto\">\n                <ion-button [disabled]=\"reserveForm.form.invalid\" (click)=\"reserveForm.onSubmit()\" color=\"light\">\n                  Rentar\n                </ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <br />\n  <br />\n  <div class=\"ion-hide-md-down\" id=\"related-place\">\n    <ion-grid padding-vertical fixed>\n      <ion-row justify-content-between>\n        <ion-col size=\"auto\">\n          <ion-text color=\"dark\">\n            <p><strong>Parqueaderos similares para rentar</strong></p>\n          </ion-text>\n        </ion-col>\n        <ion-col size=\"auto\">\n          <p [routerLink]=\"['/places']\" queryParamsHandling=\"preserve\">\n            <ion-text color=\"dark\">\n              Ver más\n            </ion-text>\n            <ion-text color=\"danger\">\n              {{filter.query}}\n            </ion-text>\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"4\" *ngFor=\"let _place of related\">\n          <app-related-place [place]=\"_place\"></app-related-place>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <app-footer class=\"ion-hide-md-down\"></app-footer>\n</ion-content>\n<ion-footer class=\"ion-hide-md-up\">\n    <ion-card-content class=\"gradient-red\">\n        <ion-row>\n            <ion-col>\n                <ion-card-title>\n                    <ion-text color=\"light\">\n                        <strong>${{place.price * filter.duration | number:'1.0-0'}}</strong>\n                    </ion-text>\n                </ion-card-title>\n                <ion-card-subtitle no-margin>\n                    <ion-text color=\"light\">\n                        Total\n                    </ion-text>\n                </ion-card-subtitle>\n            </ion-col>\n            <ion-col size=\"auto\">\n                <ion-button [disabled]=\"reserveForm.form.invalid\" (click)=\"reserveForm.onSubmit()\" color=\"light\">Rentar\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-card-content>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/reserve/reserve.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/reserve/reserve.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host app-footer {\n  background: rgba(93, 93, 93, 0.1);\n  border: none; }\n\n:host #related-place {\n  background: rgba(186, 186, 200, 0.06); }\n\n:host ion-item {\n  --border-radius: 8px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvcmVzZXJ2ZS9yZXNlcnZlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGlDQUFpQztFQUNqQyxZQUFZLEVBQUE7O0FBSHBCO0VBT1EscUNBQXFDLEVBQUE7O0FBUDdDO0VBV1Esb0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yZXNlcnZlL3Jlc2VydmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGFwcC1mb290ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDkzLCA5MywgOTMsIDAuMSk7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICB9XG5cbiAgICAjcmVsYXRlZC1wbGFjZSB7XG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMTg2LCAxODYsIDIwMCwgMC4wNik7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/reserve/reserve.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/reserve/reserve.page.ts ***!
  \***********************************************/
/*! exports provided: ReservePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservePage", function() { return ReservePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment/moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment_moment__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var ReservePage = /** @class */ (function () {
    function ReservePage(route, router, reserveService, placeService, toastCtrl, navCtrl) {
        this.route = route;
        this.router = router;
        this.reserveService = reserveService;
        this.placeService = placeService;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.place_id = null;
        this.filter = null;
        this.place = new _models__WEBPACK_IMPORTED_MODULE_5__["Place"]();
        this.displayFormat = "D MMMM, YYYY";
        this.pickerFormat = "D MMMM YYYY";
        this.monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        this.yearValues = [moment_moment__WEBPACK_IMPORTED_MODULE_6__().format("YYYY"), moment_moment__WEBPACK_IMPORTED_MODULE_6__().add(1, "year").format("YYYY")];
        this.total = 0;
    }
    ReservePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.place_id = Number(params.get("place_id"));
            if (_this.place_id && _this.filter)
                _this.get();
        });
        this.route.queryParamMap.subscribe(function (params) {
            _this.filter = new _models__WEBPACK_IMPORTED_MODULE_5__["QueryFilter"]();
            params.keys.forEach(function (key) {
                if (_this.filter.hasOwnProperty(key))
                    _this.filter[key] = params.get(key);
            });
            if (_this.place_id && _this.filter)
                _this.get();
            _this.getRelated();
        });
    };
    ReservePage.prototype.validateFormat = function () {
        if (this.place.type_id == 4) {
            this.displayFormat = "ha, DD/MM/YY";
            this.pickerFormat = "HH DD MM YYYY";
        }
        else {
            this.displayFormat = "D MMMM, YYYY";
            this.pickerFormat = "D MMMM YYYY";
        }
    };
    ReservePage.prototype.verify = function () {
        var _this = this;
        var from = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).utc().format("YYYY-MM-DD[T]HH:mm:ss.000[Z]");
        var to = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").utc().format("YYYY-MM-DD[T]HH:mm:ss.000[Z]");
        this.reserveService.verify(this.place_id, { from: from, to: to }).subscribe(function (response) {
            if (response.pagination.total > 0)
                _this.showToast("El lugar se encuentra reservado en esa fecha por favor probar con otra fecha");
            else
                _this.router.navigate([], { relativeTo: _this.route, queryParams: _this.filter, queryParamsHandling: "merge", });
        });
    };
    ReservePage.prototype.reserve = function () {
        var _this = this;
        var from = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).utc().format("YYYY-MM-DD HH:mm:ss");
        var to = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").utc().format("YYYY-MM-DD HH:mm:ss");
        this.reserveService.reserve(this.place_id, { from: from, to: to }).subscribe(function (response) {
            _this.navCtrl.navigateForward(['/user', 'history'], { queryParams: _this.filter, queryParamsHandling: "merge" });
        });
    };
    ReservePage.prototype.get = function () {
        var _this = this;
        var from = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).format("YYYY-MM-DD[T]HH:00");
        var to = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").format("YYYY-MM-DD[T]HH:00");
        this.placeService.get(this.place_id, { latitude: this.filter.latitude, longitude: this.filter.longitude, from: from, to: to }).subscribe(function (response) {
            _this.place = response;
            _this.validateFormat();
        });
    };
    ReservePage.prototype.showToast = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 2000,
                            showCloseButton: true,
                            position: 'bottom',
                            closeButtonText: 'Aceptar'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReservePage.prototype.getRelated = function () {
        var _this = this;
        this.placeService.list(this.filter.latitude, this.filter.longitude, {
            page: 1,
            limit: 6,
            status: this.filter.status,
            from: this.filter.start_at,
            to: moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").format("YYYY-MM-DD[T]HH:00"),
            filters: [
                (this.filter.type) ? "type_id:" + this.filter.type : "",
                "price:>" + this.filter.price,
                "id:!=" + this.place_id
            ].filter(function (filter) { return !!filter; }).join(";"),
            sort: this.filter.sort,
            services: null
        }).subscribe(function (response) { return _this.related = response.models; });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("reserveForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
    ], ReservePage.prototype, "reserveForm", void 0);
    ReservePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reserve',
            template: __webpack_require__(/*! ./reserve.page.html */ "./src/app/pages/reserve/reserve.page.html"),
            styles: [__webpack_require__(/*! ./reserve.page.scss */ "./src/app/pages/reserve/reserve.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services__WEBPACK_IMPORTED_MODULE_4__["ReserveService"], _services__WEBPACK_IMPORTED_MODULE_4__["PlaceService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], ReservePage);
    return ReservePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-reserve-reserve-module.js.map