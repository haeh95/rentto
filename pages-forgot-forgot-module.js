(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forgot-forgot-module"],{

/***/ "./src/app/pages/forgot/forgot.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/forgot/forgot.module.ts ***!
  \***********************************************/
/*! exports provided: ForgotPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPageModule", function() { return ForgotPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _forgot_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forgot.page */ "./src/app/pages/forgot/forgot.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _forgot_page__WEBPACK_IMPORTED_MODULE_5__["ForgotPage"]
    }
];
var ForgotPageModule = /** @class */ (function () {
    function ForgotPageModule() {
    }
    ForgotPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_forgot_page__WEBPACK_IMPORTED_MODULE_5__["ForgotPage"]]
        })
    ], ForgotPageModule);
    return ForgotPageModule;
}());



/***/ }),

/***/ "./src/app/pages/forgot/forgot.page.html":
/*!***********************************************!*\
  !*** ./src/app/pages/forgot/forgot.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n  <ion-grid no-padding fixed>\n    <ion-row align-items-center>\n      <ion-col no-padding size=\"12\">\n        <ion-row align-items-stretch>\n          <ion-col padding size=\"12\" size-md=\"5\">\n            <ion-row class=\"ion-hide-md-up\">\n              <ion-col size=\"auto\">\n                <ion-button (click)=\"goBack()\" fill=\"clear\" color=\"medium_2\">\n                  <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n                </ion-button>\n              </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-hide-md-down\" padding-top padding-horizontal>\n              <ion-col padding-top padding-horizontal>\n                <img src=\"./assets/icons/logo-red.png\">\n              </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-hide-md-down\" padding-horizontal>\n              <ion-col padding-horizontal>\n                <ion-text>\n                  <h3><strong>Olvidaste tu<br />contraseña?</strong></h3>\n                </ion-text>\n              </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-hide-md-down\" padding-horizontal>\n              <ion-col padding-horizontal>\n                <ion-text>\n                  <p>Te enviaremos instrucciones<br />por email</p>\n                </ion-text>\n              </ion-col>\n            </ion-row>\n            <ion-row padding-horizontal padding-bottom>\n              <ion-col class=\"form\" padding-bottom size=\"12\" size-md=\"9\">\n                <form #forgotForm=\"ngForm\" (ngSubmit)=\"forgot()\" novalidate>\n                  <ion-text text-center>\n                    <h3><strong>Olvidaste tu contraseña?</strong></h3>\n                  </ion-text>\n                  <ion-item margin-bottom lines=\"none\">\n                    <ion-icon slot=\"start\" src=\"./assets/svg/mail.svg\"></ion-icon>\n                    <ion-input [(ngModel)]=\"email\" name=\"email\" placeholder=\"email\" type=\"email\" required></ion-input>\n                  </ion-item>\n                  <ion-button type=\"submit\" [disabled]=\"forgotForm.form.invalid\" margin-bottom expand=\"block\"\n                    color=\"danger\">Enviar</ion-button>\n                </form>\n              </ion-col>\n            </ion-row>\n            <ion-row padding>\n              <ion-col padding>\n                <a [routerLink]=\"['/register']\">\n                  <small>\n                    <ion-text color=\"dark\">No tienes una cuenta? <ion-text color=\"danger\">Registrate</ion-text>\n                    </ion-text>\n                  </small>\n                </a>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col class=\"ion-hide-md-down\" size=\"7\">\n            <ion-button (click)=\"goBack()\" fill=\"clear\" color=\"light\">\n              <ion-icon slot=\"icon-only\" src=\"./assets/svg/close.svg\"></ion-icon>\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<app-footer class=\"ion-hide-md-down\"></app-footer>"

/***/ }),

/***/ "./src/app/pages/forgot/forgot.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/forgot/forgot.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  background-image: url('home.png');\n  background-size: cover;\n  background-position: top left;\n  background-repeat: no-repeat; }\n  @media (max-width: 820px) {\n    :host {\n      background: none; } }\n  :host ion-content {\n    --background: rgba(var(--ion-color-dark-rgb), 0.7); }\n  :host ion-content ion-grid {\n      position: absolute;\n      left: 0;\n      right: 0;\n      top: 0;\n      bottom: 0;\n      margin: auto; }\n  :host ion-content ion-grid > ion-row {\n        height: 100%; }\n  @media (max-width: 820px) {\n          :host ion-content ion-grid > ion-row {\n            align-items: stretch !important; } }\n  :host ion-content ion-grid > ion-row ion-col ion-row ion-col[size=\"12\"] {\n          border-bottom-left-radius: 12px;\n          border-top-left-radius: 12px;\n          background: var(--ion-color-light); }\n  :host ion-content ion-grid > ion-row ion-col ion-row ion-col[size=\"12\"] ion-item {\n            border: 1px solid var(--ion-color-border, #AAAAAA);\n            --border-radius: 8px;\n            border-radius: 8px; }\n  :host ion-content ion-grid > ion-row ion-col ion-row ion-col[size=\"12\"] ion-item ion-icon {\n              margin-right: 8px; }\n  :host ion-content ion-grid > ion-row ion-col ion-row ion-col[size=\"7\"] {\n          background-image: url('picture.jpg');\n          background-size: cover;\n          background-position: center;\n          background-repeat: no-repeat;\n          border-bottom-right-radius: 12px;\n          border-top-right-radius: 12px; }\n  :host ion-content ion-grid > ion-row ion-col ion-row ion-col[size=\"7\"] ion-button {\n            position: absolute;\n            top: 10px;\n            right: 5px; }\n  :host ion-content .form {\n      --padding-left: var(--ion-padding, 16px);\n      --padding-right: var(--ion-padding, 16px);\n      padding-right: var(--ion-padding, 16px);\n      padding-left: var(--ion-padding, 16px); }\n  @media (max-width: 820px) {\n      :host ion-content {\n        --background: var(--ion-color-light); }\n        :host ion-content .form {\n          --padding-left: 0;\n          --padding-rigth: 0;\n          padding-right: 0;\n          padding-left: 0; }\n        :host ion-content form {\n          padding: var(--ion-padding, 16px);\n          box-shadow: 0px 0px 5px 0px rgba(var(--ion-color-dark-rgb), 0.3);\n          border-radius: 8px; }\n        :host ion-content a {\n          text-align: center;\n          display: block; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvZm9yZ290L2ZvcmdvdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQ0FBb0Q7RUFDcEQsc0JBQXNCO0VBQ3RCLDZCQUE2QjtFQUM3Qiw0QkFBNEIsRUFBQTtFQUU1QjtJQU5KO01BT1EsZ0JBQWdCLEVBQUEsRUE0RnZCO0VBbkdEO0lBV1Esa0RBQWEsRUFBQTtFQVhyQjtNQWNZLGtCQUFrQjtNQUNsQixPQUFPO01BQ1AsUUFBUTtNQUNSLE1BQU07TUFDTixTQUFTO01BQ1QsWUFBWSxFQUFBO0VBbkJ4QjtRQXNCZ0IsWUFBWSxFQUFBO0VBRVo7VUF4QmhCO1lBeUJvQiwrQkFBK0IsRUFBQSxFQXVDdEM7RUFoRWI7VUFnQ2dDLCtCQUErQjtVQUMvQiw0QkFBNEI7VUFDNUIsa0NBQWtDLEVBQUE7RUFsQ2xFO1lBcUNvQyxrREFBa0Q7WUFDbEQsb0JBQWdCO1lBQ2hCLGtCQUFrQixFQUFBO0VBdkN0RDtjQTBDd0MsaUJBQWlCLEVBQUE7RUExQ3pEO1VBZ0RnQyxvQ0FBdUQ7VUFDdkQsc0JBQXNCO1VBQ3RCLDJCQUEyQjtVQUMzQiw0QkFBNEI7VUFDNUIsZ0NBQWdDO1VBQ2hDLDZCQUE2QixFQUFBO0VBckQ3RDtZQXdEb0Msa0JBQWtCO1lBQ2xCLFNBQVM7WUFDVCxVQUFVLEVBQUE7RUExRDlDO01Bb0VZLHdDQUFlO01BQ2YseUNBQWdCO01BQ2hCLHVDQUF1QztNQUN2QyxzQ0FBc0MsRUFBQTtFQUcxQztNQTFFUjtRQTJFWSxvQ0FBYSxFQUFBO1FBM0V6QjtVQThFZ0IsaUJBQWU7VUFDZixrQkFBZ0I7VUFDaEIsZ0JBQWdCO1VBQ2hCLGVBQWUsRUFBQTtRQWpGL0I7VUFzRmdCLGlDQUFpQztVQUdqQyxnRUFBZ0U7VUFDaEUsa0JBQWtCLEVBQUE7UUExRmxDO1VBOEZnQixrQkFBa0I7VUFDbEIsY0FBYyxFQUFBLEVBQ2pCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZm9yZ290L2ZvcmdvdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi9hc3NldHMvYmcvaG9tZS5wbmcnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IHRvcCBsZWZ0O1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5cbiAgICBAbWVkaWEgKG1heC13aWR0aDogODIwcHgpIHtcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICB9XG5cbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IpLCAwLjcpO1xuXG4gICAgICAgIGlvbi1ncmlkIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDA7XG4gICAgICAgICAgICByaWdodDogMDtcbiAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcblxuICAgICAgICAgICAgJj5pb24tcm93IHtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG5cbiAgICAgICAgICAgICAgICBAbWVkaWEgKG1heC13aWR0aDogODIwcHgpIHtcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IHN0cmV0Y2ggIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgICAgICAgICAgaW9uLXJvdyB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmW3NpemU9XCIxMlwiXSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW9uLWl0ZW0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLWJvcmRlciwgI0FBQUFBQSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJltzaXplPVwiN1wiXSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2JnL3BpY3R1cmUuanBnJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMnB4O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5mb3JtIHtcbiAgICAgICAgICAgIC0tcGFkZGluZy1sZWZ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gICAgICAgICAgICAtLXBhZGRpbmctcmlnaHQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgICAgICB9XG5cbiAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG5cbiAgICAgICAgICAgIC5mb3JtIHtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctbGVmdDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctcmlndGg6IDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZvcm0ge1xuICAgICAgICAgICAgICAgIC8vZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgMC4zKTtcbiAgICAgICAgICAgICAgICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuMyk7XG4gICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgMC4zKTtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGEge1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/forgot/forgot.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/forgot/forgot.page.ts ***!
  \*********************************************/
/*! exports provided: ForgotPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPage", function() { return ForgotPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ForgotPage = /** @class */ (function () {
    function ForgotPage(navCtrl, authService) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.email = null;
    }
    ForgotPage.prototype.ngOnInit = function () {
    };
    ForgotPage.prototype.forgot = function () {
        var _this = this;
        if (this.forgotForm.valid)
            this.authService.forgot(this.email).subscribe(function () {
                _this.navCtrl.navigateBack("login");
            });
    };
    ForgotPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("forgotForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], ForgotPage.prototype, "forgotForm", void 0);
    ForgotPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-forgot',
            template: __webpack_require__(/*! ./forgot.page.html */ "./src/app/pages/forgot/forgot.page.html"),
            styles: [__webpack_require__(/*! ./forgot.page.scss */ "./src/app/pages/forgot/forgot.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _services__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], ForgotPage);
    return ForgotPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-forgot-forgot-module.js.map