(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-place-place-module"],{

/***/ "./src/app/pages/place/place.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/place/place.module.ts ***!
  \*********************************************/
/*! exports provided: PlacePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlacePageModule", function() { return PlacePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _place_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./place.page */ "./src/app/pages/place/place.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '',
        component: _place_page__WEBPACK_IMPORTED_MODULE_5__["PlacePage"]
    }
];
var PlacePageModule = /** @class */ (function () {
    function PlacePageModule() {
    }
    PlacePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_place_page__WEBPACK_IMPORTED_MODULE_5__["PlacePage"]]
        })
    ], PlacePageModule);
    return PlacePageModule;
}());



/***/ }),

/***/ "./src/app/pages/place/place.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/place/place.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark class=\"ion-hide-md-down\"></app-header-dark>\n<div [hidden]=\"!reserve.id\" class=\"bg\">\n    <ion-grid padding-vertical fixed>\n        <ion-text color=\"dark\">\n            <h1 [style.font-size.px]=\"18\" no-margin><strong>Reserva realizada</strong></h1>\n        </ion-text>\n        <ion-row align-items-center>\n            <ion-col size=\"auto\" no-padding padding-left>\n                <ion-text color=\"medium_2\">\n                    <p [style.font-size.px]=\"16\" no-margin>Desde</p>\n                </ion-text>\n            </ion-col>\n            <ion-col size=\"2\">\n                <ion-datetime class=\"reserve\" name=\"from\" [monthNames]=\"monthNames\" [displayFormat]=\"displayFormat\"\n                    [(ngModel)]=\"reserve.from\" readonly></ion-datetime>\n            </ion-col>\n            <ion-col size=\"auto\" no-padding padding-horizontal>\n                <ion-text color=\"medium_2\">\n                    <p [style.font-size.px]=\"16\" no-margin>Hasta</p>\n                </ion-text>\n            </ion-col>\n            <ion-col size=\"2\">\n                <ion-datetime class=\"reserve\" name=\"to\" [monthNames]=\"monthNames\" [displayFormat]=\"displayFormat\"\n                    [(ngModel)]=\"reserve.to\" readonly></ion-datetime>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</div>\n<ion-content>\n    <ion-slides pager=\"true\" [options]=\"slideOpts\" class=\"ion-hide-md-up\">\n        <ion-slide *ngFor=\"let photo of place.photos\">\n            <div class=\"image-slide\"\n                [style.backgroundImage]=\"'url(' + (photo.url || './assets/img/default_2.png') + ')'\">\n            </div>\n        </ion-slide>\n    </ion-slides>\n    <ion-row id=\"header-mobile\" class=\"ion-hide-md-up\" align-items-center justify-content-between padding-bottom>\n        <ion-col no-padding size=\"auto\">\n            <ion-button no-margin (click)=\"goBack()\" fill=\"clear\" color=\"medium\">\n                <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n            </ion-button>\n        </ion-col>\n        <ion-col padding size=\"auto\">\n            <ion-badge padding-vertical [color]=\"(place.available == 1)?'primary':'danger'\">\n                {{(place.available == 1)?'Disponible':'No Disponible'}}\n            </ion-badge>\n        </ion-col>\n    </ion-row>\n    <ion-grid padding-top fixed>\n        <ion-row class=\"ion-hide-md-down\" padding-bottom>\n            <ion-col no-padding size=\"auto\">\n                <ion-button no-margin (click)=\"goBack()\" fill=\"clear\" color=\"danger\">\n                    <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n        <ion-row align-items-center padding-bottom nowrap>\n            <ion-col>\n                <ion-text color=\"medium_2\">\n                    <p [style.font-size.px]=\"14\" no-margin>{{place.interal_ubication}}</p>\n                </ion-text>\n                <ion-text color=\"dark\">\n                    <h1 [style.font-size.px]=\"50\" no-margin><strong>{{place.name}}</strong></h1>\n                </ion-text>\n                <ion-text color=\"medium_2\">\n                    <p [style.font-size.px]=\"16\" no-margin>{{place.reference}}</p>\n                </ion-text>\n            </ion-col>\n            <ion-col size=\"auto\">\n                <ion-row [style.flex-direction]=\"'column'\" justify-content-end>\n                    <ion-badge class=\"ion-hide-md-down\" padding-vertical\n                        [color]=\"(place.available == 1)?'primary':'danger'\">\n                        {{(place.available == 1)?'Disponible':'No Disponible'}}\n                    </ion-badge>\n                    <ion-row align-items-center no-padding>\n                        <ion-col>\n                            <ion-icon [hidden]=\"place.is_favorite\" (click)=\"favorite()\" color=\"medium_2\"\n                                src=\"./assets/svg/hear-outline.svg\"></ion-icon>\n                            <ion-icon [hidden]=\"!place.is_favorite\" (click)=\"favorite()\" color=\"danger\"\n                                src=\"./assets/svg/hear-filled.svg\"></ion-icon>\n                        </ion-col>\n                        <ion-col padding-vertical size=\"auto\">\n                            <ion-text color=\"danger\">\n                                <p no-margin>\n                                    <ion-icon src=\"./assets/svg/address.svg\"></ion-icon>\n                                    {{place.distance | number:'1.0-2'}} Km\n                                </p>\n                            </ion-text>\n                        </ion-col>\n                    </ion-row>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n        <ion-row class=\"ion-hide-md-down\" align-items-stretch>\n            <ion-col>\n                <div class=\"image\"\n                    [style.backgroundImage]=\"'url(' + (photo_selected.url || './assets/img/default_2.png') + ')'\"></div>\n            </ion-col>\n            <ion-col size=\"auto\"></ion-col>\n            <ion-col size=\"4\">\n                <div *ngFor=\"let photo of place.photos\" (click)=\"photo_selected = photo;\" class=\"sub-image\"\n                    [style.backgroundImage]=\"'url(' + (photo.url || './assets/img/default_2.png') + ')'\"></div>\n            </ion-col>\n        </ion-row>\n        <ion-row align-items-stretch>\n            <ion-col>\n                <ion-text color=\"dark\">\n                    <p>{{place.observation}}</p>\n                </ion-text>\n                <br />\n                <ion-row justify-content-between align-items-end>\n                    <ion-col no-padding size=\"auto\">\n                        <ion-text color=\"medium_2\">\n                            <p no-margin>Review</p>\n                        </ion-text>\n                        <app-starts [rank_value]=\"place.rank_value\"></app-starts>\n                    </ion-col>\n                    <ion-col no-padding size=\"auto\">\n                        <ion-button [routerLink]=\"['/reviews', place.id]\" queryParamsHandling=\"preserve\" fill=\"clear\"\n                            color=\"medium_2\">\n                            Ver reviews\n                            <ion-icon slot=\"end\" src=\"./assets/svg/arrow.svg\"></ion-icon>\n                        </ion-button>\n                    </ion-col>\n                </ion-row>\n                <hr class=\"ion-hide-md-up\" />\n                <ion-item no-padding margin-top lines=\"none\" color=\"noColor\">\n                    <ion-avatar slot=\"start\">\n                        <img [src]=\"place.user?.profile_photo || './assets/img/profile.png'\">\n                    </ion-avatar>\n                    <ion-label text-wrap>\n                        <h3 no-margin><strong>{{place.user?.name || 'Host Desconocido'}}</strong></h3>\n                        <ion-text color=\"medium\">\n                            <p no-margin>Host verificado</p>\n                        </ion-text>\n                    </ion-label>\n                </ion-item>\n                <hr class=\"ion-hide-md-up\" />\n                <br />\n                <br />\n                <ion-text color=\"medium_2\">\n                    <p no-margin>Tiempo minimo de alquiler:</p>\n                </ion-text>\n                <ion-text color=\"dark\">\n                    <h2 no-margin><strong>{{place.minimal_restriction}}</strong></h2>\n                </ion-text>\n                <br />\n                <ion-text color=\"medium_2\">\n                    <p no-margin>Más información:</p>\n                </ion-text>\n                <ion-text color=\"dark\">\n                    <p no-margin>{{place.description | limit:limit}}\n                        <ion-text [hidden]=\"limit == -1\" color=\"danger\"><span style=\"cursor: pointer;\"\n                                (click)=\"limit = -1;\">Ver mas</span></ion-text>\n                    </p>\n                </ion-text>\n                <br />\n                <br />\n                <ion-text color=\"medium_2\">\n                    <p no-margin>Características</p>\n                </ion-text>\n                <ion-row>\n                    <ion-col class=\"service\" *ngFor=\"let service of place.services\">\n                        <ion-text text-capitalize color=\"dark\"><span></span>{{service.name}}</ion-text>\n                    </ion-col>\n                </ion-row>\n                <br />\n                <br />\n                <hr class=\"ion-hide-md-up\" />\n                <ion-row justify-content-center>\n                    <ion-col size=\"12\" size-md=\"6\">\n                        <ion-row nowrap>\n                            <ion-col size=\"auto\">\n                                <ion-icon class=\"icon-info\" color=\"medium_2\" src=\"./assets/svg/area.svg\"></ion-icon>\n                            </ion-col>\n                            <ion-col>\n                                <ion-text color=\"medium_2\">\n                                    <p no-margin>Dimensiones</p>\n                                </ion-text>\n                                <ion-text color=\"dark\">\n                                    <p no-margin><strong>{{place.dimension}}</strong></p>\n                                </ion-text>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                    <ion-col size=\"12\" nowrap size-md=\"6\">\n                        <ion-row nowrap>\n                            <ion-col size=\"auto\">\n                                <ion-icon class=\"icon-info\" color=\"medium_2\" src=\"./assets/svg/schedule.svg\"></ion-icon>\n                            </ion-col>\n                            <ion-col>\n                                <ion-text color=\"medium_2\">\n                                    <p no-margin>Horario de entrada / salida</p>\n                                </ion-text>\n                                <ion-text color=\"dark\">\n                                    <p no-margin><strong>{{place.visit_schedule}}</strong></p>\n                                </ion-text>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                </ion-row>\n                <br />\n            </ion-col>\n            <ion-col class=\"ion-hide-md-down\" padding size=\"4\">\n                <ion-card>\n                    <ion-card-header>\n                        <ion-card-title>\n                            <ion-text color=\"dark\">\n                                <strong>${{place.price | number:'1.0-0'}}</strong>\n                            </ion-text>\n                        </ion-card-title>\n                        <ion-card-subtitle no-margin>\n                            <ion-text color=\"dark\">\n                                Por {{place.price_duration}}\n                            </ion-text>\n                        </ion-card-subtitle>\n                    </ion-card-header>\n                    <ion-card-content>\n                        <form #verifyForm=\"ngForm\" (ngSubmit)=\"verify()\" novalidate>\n                            <ion-text color=\"medium_2\">\n                                <p no-margin>Verifica disponibilidad</p>\n                            </ion-text>\n                            <ion-row padding-vertical>\n                                <ion-col no-padding>\n                                    <ion-datetime name=\"start_at\" [yearValues]=\"yearValues\" [monthNames]=\"monthNames\"\n                                        [pickerFormat]=\"pickerFormat\" [displayFormat]=\"displayFormat\" [min]=\"start_min\"\n                                        [(ngModel)]=\"filter.start_at\" placeholder=\"Fecha de inicio\" doneText=\"Aceptar\"\n                                        cancelText=\"Cancelar\" required>\n                                    </ion-datetime>\n                                </ion-col>\n                                <ion-col size=\"auto\"></ion-col>\n                                <ion-col no-padding>\n                                    <ion-input name=\"duration\" [(ngModel)]=\"filter.duration\" placeholder=\"Duración\"\n                                        inputmode=\"number\" type=\"number\" required></ion-input>\n                                </ion-col>\n                            </ion-row>\n                        </form>\n                    </ion-card-content>\n                    <ion-card-content class=\"gradient-red\">\n                        <ion-row>\n                            <ion-col>\n                                <ion-card-title>\n                                    <ion-text color=\"light\">\n                                        <strong>${{place.price * filter.duration | number:'1.0-0'}}</strong>\n                                    </ion-text>\n                                </ion-card-title>\n                                <ion-card-subtitle no-margin>\n                                    <ion-text color=\"light\">\n                                        Total\n                                    </ion-text>\n                                </ion-card-subtitle>\n                            </ion-col>\n                            <ion-col size=\"auto\">\n                                <ion-button [disabled]=\"verifyForm.form.invalid\" (click)=\"verifyForm.onSubmit()\"\n                                    color=\"light\">Verificar</ion-button>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <hr />\n    <ion-grid padding-vertical fixed>\n        <ion-row padding-bottom align-items-center>\n            <ion-col>\n                <ion-text color=\"dark\">\n                    <p><strong>Preguntas y respuestas</strong></p>\n                </ion-text>\n                <form #questionForm=\"ngForm\" (ngSubmit)=\"ask()\" novalidate>\n                    <ion-row [hidden]=\"!isLogin()\">\n                        <ion-col no-padding>\n                            <ion-input placeholder=\"Realiza tu pregunta\" [(ngModel)]=\"question\" name=\"question\"\n                                required></ion-input>\n                        </ion-col>\n                        <ion-col size=\"auto\"></ion-col>\n                        <ion-col size=\"auto\" no-padding>\n                            <ion-button type=\"submit\" no-margin color=\"danger\">Preguntar</ion-button>\n                        </ion-col>\n                    </ion-row>\n                </form>\n                <div padding-vertical>\n                    <app-question [question]=\"question\" *ngFor=\"let question of questions\"></app-question>\n                </div>\n                <ion-row [hidden]=\"total_question == 0\" padding-horizontal>\n                    <ion-col padding-horizontal>\n                        <ion-button [routerLink]=\"['/questions', place.id]\" queryParamsHandling=\"preserve\"\n                            fill=\"outline\" color=\"danger\">Ver más preguntas y respuestas ({{total_question}})\n                        </ion-button>\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n            <ion-col class=\"ion-hide-md-down\" no-padding size=\"4\"></ion-col>\n        </ion-row>\n    </ion-grid>\n    <div class=\"ion-hide-md-down bg\">\n        <ion-grid padding-vertical fixed>\n            <ion-row justify-content-between>\n                <ion-col size=\"auto\">\n                    <ion-text color=\"dark\">\n                        <p><strong>Parqueaderos similares para rentar</strong></p>\n                    </ion-text>\n                </ion-col>\n                <ion-col size=\"auto\">\n                    <p [routerLink]=\"['/places']\" queryParamsHandling=\"preserve\">\n                        <ion-text color=\"dark\">\n                            Ver más\n                        </ion-text>\n                        <ion-text color=\"danger\">\n                            {{filter.query}}\n                        </ion-text>\n                    </p>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col size=\"4\" *ngFor=\"let _place of related\">\n                    <app-related-place [place]=\"_place\"></app-related-place>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n    <app-footer class=\"ion-hide-md-down\"></app-footer>\n</ion-content>\n<ion-footer class=\"ion-hide-md-up\">\n    <ion-card-content class=\"gradient-red\">\n        <ion-row>\n            <ion-col>\n                <ion-card-title>\n                    <ion-text color=\"light\">\n                        <strong>${{place.price | number:'1.0-0'}}</strong>\n                    </ion-text>\n                </ion-card-title>\n                <ion-card-subtitle no-margin>\n                    <ion-text color=\"light\">\n                        Por {{place.price_duration}}\n                    </ion-text>\n                </ion-card-subtitle>\n            </ion-col>\n            <ion-col size=\"auto\">\n                <ion-button [disabled]=\"verifyForm.form.invalid\" (click)=\"verifyForm.onSubmit()\" color=\"light\">Verificar\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-card-content>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/place/place.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/place/place.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host div.image {\n  display: block;\n  width: 100%;\n  height: 440px;\n  margin-bottom: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n:host div.image-slide {\n  display: block;\n  width: 100%;\n  height: 240px;\n  margin-bottom: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n:host div.sub-image {\n  display: block;\n  width: 100%;\n  height: 215px;\n  margin-bottom: 10px;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n:host #header-mobile {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 90; }\n\n:host ion-slides {\n  top: 0; }\n\n:host app-footer {\n  background: rgba(93, 93, 93, 0.1);\n  border: none; }\n\n:host ion-icon.icon-info {\n  height: 30px;\n  width: 26px; }\n\n:host ion-col.service {\n  line-height: 1; }\n\n:host ion-col.service span {\n    display: inline-block;\n    height: 6px;\n    width: 6px;\n    margin: 0px 12px 2px 0px;\n    border-radius: 50%;\n    background: var(--ion-color-dark); }\n\n:host ion-card {\n  width: 100%;\n  margin: 0; }\n\n:host ion-datetime {\n  border: 1px solid #F0F1F3;\n  border-radius: 8px;\n  width: 100%; }\n\n:host ion-datetime.reserve {\n    background: var(--ion-color-light); }\n\n:host ion-input {\n  border: 1px solid #F0F1F3;\n  border-radius: 8px;\n  width: 100%; }\n\n:host .bg {\n  background: rgba(186, 186, 200, 0.06); }\n\n:host hr {\n  background: var(--ion-color-border, #AAAAAA); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvcGxhY2UvcGxhY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRVEsY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0Qiw0QkFBNEI7RUFDNUIsMkJBQTJCLEVBQUE7O0FBUm5DO0VBWVEsY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0Qiw0QkFBNEI7RUFDNUIsMkJBQTJCLEVBQUE7O0FBbEJuQztFQXNCUSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLDRCQUE0QjtFQUM1QiwyQkFBMkIsRUFBQTs7QUE1Qm5DO0VBZ0NRLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFFBQVE7RUFDUixXQUFXLEVBQUE7O0FBcENuQjtFQXdDUSxNQUFNLEVBQUE7O0FBeENkO0VBNENRLGlDQUFpQztFQUNqQyxZQUFZLEVBQUE7O0FBN0NwQjtFQWlEUSxZQUFZO0VBQ1osV0FBVyxFQUFBOztBQWxEbkI7RUFzRFEsY0FBYyxFQUFBOztBQXREdEI7SUF5RFkscUJBQXFCO0lBQ3JCLFdBQVc7SUFDWCxVQUFVO0lBQ1Ysd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixpQ0FBaUMsRUFBQTs7QUE5RDdDO0VBbUVRLFdBQVc7RUFDWCxTQUFTLEVBQUE7O0FBcEVqQjtFQXdFUSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUExRW5CO0lBNkVZLGtDQUFrQyxFQUFBOztBQTdFOUM7RUFrRlEseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixXQUFXLEVBQUE7O0FBcEZuQjtFQXdGUSxxQ0FBcUMsRUFBQTs7QUF4RjdDO0VBNEZRLDRDQUE0QyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGxhY2UvcGxhY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGRpdi5pbWFnZSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiA0NDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIH1cblxuICAgIGRpdi5pbWFnZS1zbGlkZSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyNDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIH1cblxuICAgIGRpdi5zdWItaW1hZ2Uge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMjE1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAjaGVhZGVyLW1vYmlsZSB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICByaWdodDogMDtcbiAgICAgICAgei1pbmRleDogOTA7XG4gICAgfVxuXG4gICAgaW9uLXNsaWRlcyB7XG4gICAgICAgIHRvcDogMDtcbiAgICB9XG5cbiAgICBhcHAtZm9vdGVyIHtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSg5MywgOTMsIDkzLCAwLjEpO1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxuXG4gICAgaW9uLWljb24uaWNvbi1pbmZvIHtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICB3aWR0aDogMjZweDtcbiAgICB9XG5cbiAgICBpb24tY29sLnNlcnZpY2Uge1xuICAgICAgICBsaW5lLWhlaWdodDogMTtcblxuICAgICAgICBzcGFuIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgIGhlaWdodDogNnB4O1xuICAgICAgICAgICAgd2lkdGg6IDZweDtcbiAgICAgICAgICAgIG1hcmdpbjogMHB4IDEycHggMnB4IDBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG5cbiAgICBpb24tZGF0ZXRpbWUge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjRjBGMUYzO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgICAgICYucmVzZXJ2ZSB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgaW9uLWlucHV0IHtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0YwRjFGMztcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cbiAgICAuYmcge1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDE4NiwgMTg2LCAyMDAsIDAuMDYpO1xuICAgIH1cblxuICAgIGhyIHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWJvcmRlciwgI0FBQUFBQSk7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/place/place.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/place/place.page.ts ***!
  \*******************************************/
/*! exports provided: PlacePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlacePage", function() { return PlacePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment/moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment_moment__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var PlacePage = /** @class */ (function () {
    function PlacePage(toastCtrl, navCtrl, route, questionService, reserveService, placeService, favoriteService) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.route = route;
        this.questionService = questionService;
        this.reserveService = reserveService;
        this.placeService = placeService;
        this.favoriteService = favoriteService;
        this.slideOpts = {
            init: true,
            speed: 400,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            }
        };
        this.place_id = null;
        this.reserve = null;
        this.filter = null;
        this.place = new _models__WEBPACK_IMPORTED_MODULE_4__["Place"]();
        this.photo_selected = new _models__WEBPACK_IMPORTED_MODULE_4__["Photo"]();
        this.total_question = 0;
        this.displayFormat = "D MMMM, YYYY";
        this.pickerFormat = "D MMMM YYYY";
        this.monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        this.yearValues = [moment_moment__WEBPACK_IMPORTED_MODULE_6__().format("YYYY"), moment_moment__WEBPACK_IMPORTED_MODULE_6__().add(1, "year").format("YYYY")];
        this.total = 0;
        this.limit = 120;
    }
    PlacePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParamMap.subscribe(function (params) {
            _this.filter = new _models__WEBPACK_IMPORTED_MODULE_4__["QueryFilter"]();
            _this.reserve = new _models__WEBPACK_IMPORTED_MODULE_4__["Reserve"]();
            params.keys.forEach(function (key) {
                if (_this.filter.hasOwnProperty(key))
                    _this.filter[key] = params.get(key);
                if (_this.reserve.hasOwnProperty(key))
                    _this.reserve[key] = params.get(key);
            });
            if (!!_this.place_id && _this.filter)
                _this.get();
        });
        this.route.paramMap.subscribe(function (params) {
            _this.place_id = Number(params.get("place_id"));
            if (!!_this.place_id && _this.filter)
                _this.get();
            if (!!_this.place_id)
                _this.getQuestion();
            _this.getRelated();
        });
    };
    PlacePage.prototype.favorite = function () {
        var _this = this;
        if (localStorage.getItem("token")) {
            if (this.place.is_favorite) {
                this.favoriteService.remove(this.place.id).subscribe(function () { return _this.place.is_favorite = false; });
            }
            else {
                this.favoriteService.add(this.place.id).subscribe(function () { return _this.place.is_favorite = true; });
            }
        }
        else {
            this.navCtrl.navigateForward("login");
        }
    };
    PlacePage.prototype.verify = function () {
        var _this = this;
        var from = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).utc().format("YYYY-MM-DD[T]HH:mm:ss.000[Z]");
        var to = moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").utc().format("YYYY-MM-DD[T]HH:mm:ss.000[Z]");
        this.reserveService.verify(this.place_id, { from: from, to: to }).subscribe(function (response) {
            if (response.pagination.total == 0)
                _this.navCtrl.navigateForward(["/reserve", _this.place.id], { queryParams: _this.filter, queryParamsHandling: "merge" });
            else
                _this.showToast("El lugar se encuentra reservado en esa fecha por favor probar con otra fecha");
        });
    };
    PlacePage.prototype.ask = function () {
        var _this = this;
        if (this.questionForm.form.valid)
            this.questionService.add(this.place.id, { question: this.question }).subscribe(function () { return _this.getQuestion(); });
    };
    PlacePage.prototype.isLogin = function () {
        return !!localStorage.getItem("token");
    };
    PlacePage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    PlacePage.prototype.validateFormat = function () {
        if (this.filter.type == 4) {
            this.displayFormat = "ha, DD/MM/YY";
            this.pickerFormat = "HH DD MM YYYY";
        }
        else {
            this.displayFormat = "D MMMM, YYYY";
            this.pickerFormat = "D MMMM YYYY";
        }
    };
    PlacePage.prototype.get = function () {
        var _this = this;
        this.placeService.get(this.place_id, { latitude: this.filter.latitude, longitude: this.filter.longitude, from: this.filter.start_at || this.reserve.from, to: moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").format("YYYY-MM-DD[T]HH:00") || this.reserve.to }).subscribe(function (response) {
            _this.place = response;
            _this.photo_selected = _this.place.photos[0] || _this.photo_selected;
            for (var i = _this.place.photos.length; i < 2; i++)
                _this.place.photos.push(new _models__WEBPACK_IMPORTED_MODULE_4__["Photo"]());
            _this.validateFormat();
        });
    };
    PlacePage.prototype.getQuestion = function () {
        var _this = this;
        this.questionService.list(this.place_id, { page: 1, limit: 3, filters: null, sort: null }).subscribe(function (response) {
            _this.questions = response.models;
            _this.total_question = response.pagination.total;
        });
    };
    PlacePage.prototype.showToast = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: message,
                            duration: 2000,
                            showCloseButton: true,
                            position: 'bottom',
                            closeButtonText: 'Aceptar'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PlacePage.prototype.getRelated = function () {
        var _this = this;
        this.placeService.list(this.filter.latitude, this.filter.longitude, {
            page: 1,
            limit: 6,
            status: this.filter.status,
            from: this.filter.start_at || this.reserve.from,
            to: moment_moment__WEBPACK_IMPORTED_MODULE_6__(this.filter.start_at).add(this.filter.duration, (this.filter.type == 4) ? "hour" : "month").format("YYYY-MM-DD[T]HH:00") || this.reserve.to,
            filters: [
                (this.filter.type) ? "type_id:" + this.filter.type : "",
                "price:>" + this.filter.price,
                "id:!=" + this.place_id
            ].filter(function (filter) { return !!filter; }).join(";"),
            sort: this.filter.sort,
            services: null
        }).subscribe(function (response) { return _this.related = response.models; });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("questionForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], PlacePage.prototype, "questionForm", void 0);
    PlacePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-place',
            template: __webpack_require__(/*! ./place.page.html */ "./src/app/pages/place/place.page.html"),
            styles: [__webpack_require__(/*! ./place.page.scss */ "./src/app/pages/place/place.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _services__WEBPACK_IMPORTED_MODULE_5__["QuestionService"], _services__WEBPACK_IMPORTED_MODULE_5__["ReserveService"], _services__WEBPACK_IMPORTED_MODULE_5__["PlaceService"], _services__WEBPACK_IMPORTED_MODULE_5__["FavoriteService"]])
    ], PlacePage);
    return PlacePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-place-place-module.js.map