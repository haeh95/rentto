(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-user-user-module"],{

/***/ "./src/app/pages/user/user.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/user/user.module.ts ***!
  \*******************************************/
/*! exports provided: UserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageModule", function() { return UserPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_router_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.router.module */ "./src/app/pages/user/user.router.module.ts");
/* harmony import */ var _user_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user.page */ "./src/app/pages/user/user.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var UserPageModule = /** @class */ (function () {
    function UserPageModule() {
    }
    UserPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _user_router_module__WEBPACK_IMPORTED_MODULE_4__["UserPageRoutingModule"]],
            declarations: [_user_page__WEBPACK_IMPORTED_MODULE_5__["UserPage"]]
        })
    ], UserPageModule);
    return UserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/user/user.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/user/user.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark class=\"ion-hide-md-down\"></app-header-dark>\n<ion-toolbar class=\"ion-hide-md-down\" color=\"bar\">\n    <ion-grid no-padding fixed>\n        <ion-row align-items-center justify-content-start>\n            <ion-col no-padding size=\"4\">\n                <ion-item lines=\"none\" color=\"noColor\">\n                    <ion-avatar slot=\"start\">\n                        <div class=\"image\"\n                            [style.backgroundImage]=\"'url(' + (user.profile_photo || './assets/img/profile.png') + ')'\">\n                        </div>\n                        <ion-icon (click)=\"picture.click()\" id=\"add_photo\" src=\"./assets/svg/add.svg\"></ion-icon>\n                    </ion-avatar>\n                    <input id=\"picture\" name=\"picture\" #picture type=\"file\" accept=\"image/*\" (change)=\"upload($event)\"\n                        hidden>\n                    <ion-label text-wrap>\n                        <ion-text color=\"medium\">\n                            <p no-margin>Hola</p>\n                        </ion-text>\n                        <h1 no-margin><strong>{{user.name}}</strong></h1>\n                    </ion-label>\n                </ion-item>\n            </ion-col>\n            <ion-col no-padding align-self-end size=\"5\">\n                <ion-row align-items-stretch justify-content-around>\n                    <ion-col class=\"ion-text-center\" padding-bottom routerLinkActive=\"actived\"\n                        #profileStatus=\"routerLinkActive\" [routerLink]=\"['/user', 'profile']\"\n                        queryParamsHandling=\"preserve\">\n                        <ion-text [color]=\"(profileStatus.isActive) ? 'dark' : 'medium'\"><strong>Perfil</strong>\n                        </ion-text>\n                    </ion-col>\n                    <ion-col class=\"ion-text-center\" padding-bottom routerLinkActive=\"actived\"\n                        #chatStatus=\"routerLinkActive\" [routerLink]=\"['/user', 'chats']\" queryParamsHandling=\"preserve\">\n                        <ion-text [color]=\"(chatStatus.isActive) ? 'dark' : 'medium'\"><strong>Chat</strong></ion-text>\n                    </ion-col>\n                    <ion-col class=\"ion-text-center\" padding-bottom routerLinkActive=\"actived\"\n                        #historyStatus=\"routerLinkActive\" [routerLink]=\"['/user', 'history']\"\n                        queryParamsHandling=\"preserve\">\n                        <ion-text [color]=\"(historyStatus.isActive) ? 'dark' : 'medium'\"><strong>Historial</strong>\n                        </ion-text>\n                    </ion-col>\n                    <ion-col class=\"ion-text-center\" padding-bottom routerLinkActive=\"actived\"\n                        #listStatus=\"routerLinkActive\" [routerLink]=\"['/user', 'list']\"\n                        queryParamsHandling=\"preserve\">\n                        <ion-text [color]=\"(listStatus.isActive) ? 'dark' : 'medium'\"><strong>Listadas</strong>\n                        </ion-text>\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-toolbar>\n<ion-content>\n    <ion-router-outlet></ion-router-outlet>\n</ion-content>\n<app-footer class=\"ion-hide-md-down\"></app-footer>"

/***/ }),

/***/ "./src/app/pages/user/user.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/user/user.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .actived {\n  border-bottom: 2px solid var(--ion-color-danger); }\n\n:host app-footer {\n  background: rgba(93, 93, 93, 0.1);\n  border: none; }\n\n:host #add_photo {\n  color: var(--ion-color-light);\n  background: var(--ion-color-danger);\n  border-radius: 50%;\n  padding: 5px;\n  position: absolute;\n  bottom: 10px;\n  left: 35px;\n  width: 14px;\n  height: 14px;\n  cursor: pointer; }\n\n:host .image {\n  display: block;\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n  background-color: var(--ion-color-light);\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvdXNlci91c2VyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGdEQUFnRCxFQUFBOztBQUZ4RDtFQU1RLGlDQUFpQztFQUNqQyxZQUFZLEVBQUE7O0FBUHBCO0VBV1EsNkJBQTZCO0VBQzdCLG1DQUFtQztFQUNuQyxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osVUFBVTtFQUNWLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZSxFQUFBOztBQXBCdkI7RUF3QlEsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHdDQUF3QztFQUN4QyxzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLDJCQUEyQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdXNlci91c2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICAuYWN0aXZlZCB7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcbiAgICB9XG5cbiAgICBhcHAtZm9vdGVyIHtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSg5MywgOTMsIDkzLCAwLjEpO1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxuXG4gICAgI2FkZF9waG90byB7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAxMHB4O1xuICAgICAgICBsZWZ0OiAzNXB4O1xuICAgICAgICB3aWR0aDogMTRweDtcbiAgICAgICAgaGVpZ2h0OiAxNHB4O1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuXG4gICAgLmltYWdlIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/user/user.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/user/user.page.ts ***!
  \*****************************************/
/*! exports provided: UserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPage", function() { return UserPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserPage = /** @class */ (function () {
    function UserPage(generalService, userService) {
        this.generalService = generalService;
        this.userService = userService;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
    }
    UserPage.prototype.ngOnInit = function () {
    };
    UserPage.prototype.upload = function (event) {
        var _this = this;
        if (event.target.files && event.target.files.length > 0) {
            this.file = event.target.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                _this.user.profile_photo = e.target['result'];
                _this.uploadImage();
            };
            reader.readAsDataURL(this.file);
        }
    };
    UserPage.prototype.uploadImage = function () {
        var _this = this;
        this.generalService.upload(this.file).subscribe(function (response) {
            _this.user.profile_photo = response.url;
            _this.userService.edit(_this.user).subscribe();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('picture'),
        __metadata("design:type", Object)
    ], UserPage.prototype, "picture", void 0);
    UserPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.page.html */ "./src/app/pages/user/user.page.html"),
            styles: [__webpack_require__(/*! ./user.page.scss */ "./src/app/pages/user/user.page.scss")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_1__["GeneralService"], _services__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], UserPage);
    return UserPage;
}());



/***/ }),

/***/ "./src/app/pages/user/user.router.module.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/user/user.router.module.ts ***!
  \**************************************************/
/*! exports provided: UserPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageRoutingModule", function() { return UserPageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.page */ "./src/app/pages/user/user.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: "",
        component: _user_page__WEBPACK_IMPORTED_MODULE_2__["UserPage"],
        children: [
            {
                path: "profile",
                children: [
                    {
                        path: "",
                        loadChildren: "../profile-menu/profile-menu.module#ProfileMenuPageModule"
                    }
                ]
            },
            {
                path: "chats",
                children: [
                    {
                        path: "",
                        loadChildren: "../chats-menu/chats-menu.module#ChatsMenuPageModule"
                    }
                ]
            },
            {
                path: "history",
                children: [
                    {
                        path: "",
                        loadChildren: "../history/history.module#HistoryPageModule"
                    }
                ]
            },
            {
                path: 'list',
                children: [
                    {
                        path: "",
                        loadChildren: '../list-menu/list-menu.module#ListMenuPageModule'
                    }
                ]
            },
            { path: "", redirectTo: "/user/profile/info", pathMatch: "full" }
        ]
    },
    {
        path: "",
        redirectTo: "/user/profile/info",
        pathMatch: "full"
    }
];
var UserPageRoutingModule = /** @class */ (function () {
    function UserPageRoutingModule() {
    }
    UserPageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UserPageRoutingModule);
    return UserPageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-user-user-module.js.map