(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chats-menu-chats-menu-module"],{

/***/ "./src/app/pages/chats-menu/chats-menu.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/chats-menu/chats-menu.module.ts ***!
  \*******************************************************/
/*! exports provided: ChatsMenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsMenuPageModule", function() { return ChatsMenuPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chats_menu_router_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chats-menu.router.module */ "./src/app/pages/chats-menu/chats-menu.router.module.ts");
/* harmony import */ var _chats_menu_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chats-menu.page */ "./src/app/pages/chats-menu/chats-menu.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ChatsMenuPageModule = /** @class */ (function () {
    function ChatsMenuPageModule() {
    }
    ChatsMenuPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _chats_menu_router_module__WEBPACK_IMPORTED_MODULE_4__["ChatsPageRoutingModule"]],
            declarations: [_chats_menu_page__WEBPACK_IMPORTED_MODULE_5__["ChatsMenuPage"]]
        })
    ], ChatsMenuPageModule);
    return ChatsMenuPageModule;
}());



/***/ }),

/***/ "./src/app/pages/chats-menu/chats-menu.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/chats-menu/chats-menu.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid no-padding fixed>\n    <ion-row aling-items-stretch>\n        <ion-col no-padding padding-top [ngClass]=\"{ 'ion-hide-md-down': chatOutlet.isActivated }\" size=\"12\"\n            size-md=\"3\">\n            <ion-item lines=\"none\" margin-top color=\"noColor\">\n                <ion-label text-wrap>\n                    <ion-text color=\"medium\">\n                        <p no-margin>Tu</p>\n                    </ion-text>\n                    <h1 no-margin><strong>Chat</strong></h1>\n                </ion-label>\n            </ion-item>\n            <ion-list>\n                <ion-item *ngFor=\"let chat of chats\" routerLinkActive=\"actived\"\n                    [routerLink]=\"['/user', 'chats', 'chat', chat.id]\" [queryParams]=\"chat.user_to_show\"\n                    queryParamsHandling=\"merge\" no-padding lines=\"none\" color=\"noColor\">\n                    <ion-avatar slot=\"start\">\n                        <div class=\"image\"\n                            [style.backgroundImage]=\"'url(' + (chat.user_to_show?.profile_photo || './assets/img/profile.png') + ')'\">\n                        </div>\n                    </ion-avatar>\n                    <ion-label text-wrap>\n                        <h3 no-margin><strong>{{chat.user_to_show?.name || 'Host Desconocido'}}</strong></h3>\n                        <ion-text color=\"medium\">\n                            <p no-margin nowrap>{{chat.message.message}}</p>\n                        </ion-text>\n                    </ion-label>\n                    <ion-note color=\"medium_2\" slot=\"end\">{{chat.created_at | date:\"h:mm a\"}}</ion-note>\n                </ion-item>\n            </ion-list>\n            <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n                <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas chats...\">\n                </ion-infinite-scroll-content>\n            </ion-infinite-scroll>\n        </ion-col>\n        <ion-col [ngClass]=\"{ 'ion-hide-md-down': !chatOutlet.isActivated }\" no-padding size=\"12\" size-md=\"8\">\n            <ion-row justify-content-center [hidden]=\"chatOutlet.isActivated && chats.length > 0\">\n                <ion-col size=\"4\">\n                    <ion-card padding color=\"medium_3\">\n                        Elige uno de los chats que iniciaste para continuar la conversación.\n                    </ion-card>\n                </ion-col>\n            </ion-row>\n            <ion-row justify-content-center [hidden]=\"chatOutlet.isActivated && chats.length == 0\">\n                <ion-col size=\"4\">\n                    <ion-card padding color=\"medium_3\">\n                        Aún no has iniciado chats en Rentto!\n                    </ion-card>\n                </ion-col>\n            </ion-row>\n            <router-outlet #chatOutlet=\"outlet\"></router-outlet>\n        </ion-col>\n    </ion-row>\n</ion-grid>\n<app-tab-footer class=\"ion-hide-md-up\"></app-tab-footer>"

/***/ }),

/***/ "./src/app/pages/chats-menu/chats-menu.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/chats-menu/chats-menu.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-grid {\n  position: relative;\n  height: calc(100% - 128px); }\n  :host ion-grid ion-row {\n    height: 100%; }\n  :host ion-grid ion-row ion-col[size-md=\"3\"] {\n      border-right: 2px solid var(--ion-color-border, #AAAAAA); }\n  @media (max-width: 820px) {\n        :host ion-grid ion-row ion-col[size-md=\"3\"] {\n          padding-left: 12px;\n          padding-right: 12px;\n          --padding-left: 12px;\n          --padding-right: 12px; } }\n  :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item {\n        border-bottom: 1px solid var(--ion-color-border, #AAAAAA); }\n  @media (max-width: 820px) {\n          :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item {\n            --inner-padding-end: 0;\n            --padding-right: 0px; }\n            :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item ion-note {\n              margin-inline-end: 0;\n              -webkit-margin-end: 0; } }\n  :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item .image {\n          display: block;\n          width: 35px;\n          height: 35px;\n          border-radius: 50%;\n          margin-right: 5px;\n          background-color: var(--ion-color-light);\n          background-size: cover;\n          background-repeat: no-repeat;\n          background-position: center; }\n  :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item.actived {\n          border-right: 2px solid var(--ion-color-danger); }\n  :host ion-grid ion-row ion-col[size-md=\"3\"] ion-list ion-item:last-child {\n          border-bottom-width: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvY2hhdHMtbWVudS9jaGF0cy1tZW51LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGtCQUFrQjtFQUNsQiwwQkFBMEIsRUFBQTtFQUhsQztJQU1ZLFlBQVksRUFBQTtFQU54QjtNQVVvQix3REFBd0QsRUFBQTtFQUV4RDtRQVpwQjtVQWF3QixrQkFBa0I7VUFDbEIsbUJBQW1CO1VBQ25CLG9CQUFlO1VBQ2YscUJBQWdCLEVBQUEsRUFzQ3ZCO0VBdERqQjtRQXFCNEIseURBQXlELEVBQUE7RUFFekQ7VUF2QjVCO1lBd0JnQyxzQkFBb0I7WUFDcEIsb0JBQWdCLEVBQUE7WUF6QmhEO2NBNEJvQyxvQkFBb0I7Y0FDcEIscUJBQXFCLEVBQUEsRUFDeEI7RUE5QmpDO1VBa0NnQyxjQUFjO1VBQ2QsV0FBVztVQUNYLFlBQVk7VUFDWixrQkFBa0I7VUFDbEIsaUJBQWlCO1VBQ2pCLHdDQUF3QztVQUN4QyxzQkFBc0I7VUFDdEIsNEJBQTRCO1VBQzVCLDJCQUEyQixFQUFBO0VBMUMzRDtVQThDZ0MsK0NBQStDLEVBQUE7RUE5Qy9FO1VBa0RnQyxzQkFBc0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NoYXRzLW1lbnUvY2hhdHMtbWVudS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW9uLWdyaWQge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGhlaWdodDogY2FsYygxMDAlIC0gMTI4cHgpO1xuXG4gICAgICAgIGlvbi1yb3cge1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgICAgICAmW3NpemUtbWQ9XCIzXCJdIHtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWJvcmRlciwgI0FBQUFBQSk7XG5cbiAgICAgICAgICAgICAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMnB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgLS1wYWRkaW5nLWxlZnQ6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAtLXBhZGRpbmctcmlnaHQ6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpb24tbGlzdCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpb24taXRlbSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1ib3JkZXIsICNBQUFBQUEpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC0tcGFkZGluZy1yaWdodDogMHB4O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlvbi1ub3RlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1pbmxpbmUtZW5kOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1tYXJnaW4tZW5kOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmltYWdlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmLmFjdGl2ZWQge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tLXdpZHRoOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/chats-menu/chats-menu.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/chats-menu/chats-menu.page.ts ***!
  \*****************************************************/
/*! exports provided: ChatsMenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsMenuPage", function() { return ChatsMenuPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatsMenuPage = /** @class */ (function () {
    function ChatsMenuPage(chatService) {
        this.chatService = chatService;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_3__["Pagination"]();
    }
    ChatsMenuPage.prototype.ngOnInit = function () {
        this.list();
    };
    ChatsMenuPage.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.chats = response.models.map(function (chat) {
                chat.user_to_show = (chat.creator_id == _this.user.id) ? chat.responder : chat.creator;
                return chat;
            });
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatsMenuPage.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.chats = _this.chats.concat(response.models.map(function (chat) {
                chat.user_to_show = (chat.creator_id == _this.user.id) ? chat.responder : chat.creator;
                return chat;
            }));
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatsMenuPage.prototype.load = function (callback) {
        this.chatService.list({
            page: this.pagination.page,
            limit: 20,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], ChatsMenuPage.prototype, "infiniteScroll", void 0);
    ChatsMenuPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chats-menu',
            template: __webpack_require__(/*! ./chats-menu.page.html */ "./src/app/pages/chats-menu/chats-menu.page.html"),
            styles: [__webpack_require__(/*! ./chats-menu.page.scss */ "./src/app/pages/chats-menu/chats-menu.page.scss")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], ChatsMenuPage);
    return ChatsMenuPage;
}());



/***/ }),

/***/ "./src/app/pages/chats-menu/chats-menu.router.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/chats-menu/chats-menu.router.module.ts ***!
  \**************************************************************/
/*! exports provided: ChatsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPageRoutingModule", function() { return ChatsPageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chats_menu_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chats-menu.page */ "./src/app/pages/chats-menu/chats-menu.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: "",
        component: _chats_menu_page__WEBPACK_IMPORTED_MODULE_2__["ChatsMenuPage"],
        children: [
            {
                path: "chat/:chat_id",
                children: [
                    {
                        path: "",
                        loadChildren: "../chat/chat.module#ChatPageModule"
                    }
                ]
            }
        ]
    }
];
var ChatsPageRoutingModule = /** @class */ (function () {
    function ChatsPageRoutingModule() {
    }
    ChatsPageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChatsPageRoutingModule);
    return ChatsPageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=chats-menu-chats-menu-module.js.map