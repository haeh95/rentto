(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-support-support-module"],{

/***/ "./src/app/pages/support/support.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.module.ts ***!
  \*************************************************/
/*! exports provided: SupportPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportPageModule", function() { return SupportPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _support_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./support.page */ "./src/app/pages/support/support.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_modals_modals_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modals/modals.module */ "./src/app/modals/modals.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '',
        component: _support_page__WEBPACK_IMPORTED_MODULE_5__["SupportPage"]
    }
];
var SupportPageModule = /** @class */ (function () {
    function SupportPageModule() {
    }
    SupportPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                src_app_modals_modals_module__WEBPACK_IMPORTED_MODULE_7__["ModalsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_support_page__WEBPACK_IMPORTED_MODULE_5__["SupportPage"]]
        })
    ], SupportPageModule);
    return SupportPageModule;
}());



/***/ }),

/***/ "./src/app/pages/support/support.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark></app-header-dark>\n<ion-content padding-top>\n  <ion-grid padding-top fixed>\n    <ion-row padding-bottom nowrap>\n      <ion-col no-padding>\n        <ion-text color=\"dark\">\n          <h1 no-margin><strong>¡Hola, {{user.name}}!</strong></h1>\n        </ion-text>\n        <ion-text color=\"dark\">\n          <h1 no-margin>¿Como podemos ayudarte?</h1>\n        </ion-text>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <hr />\n  <ion-grid padding-vertical fixed>\n    <ion-row padding-bottom align-items-center>\n      <ion-col no-padding size=\"5\">\n        <ion-label color=\"dark\">\n          <strong>Preguntas Frecuentes</strong>\n        </ion-label>\n        <br />\n        <br />\n        <ion-item class=\"page\" lines=\"none\" (click)=\"selected_page = page;\" *ngFor=\"let page of pages\">\n          <ion-icon slot=\"start\" src=\"./assets/svg/page.svg\"></ion-icon>\n          <ion-label color=\"dark\">\n            <strong>{{page.title}}</strong>\n          </ion-label>\n        </ion-item>\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas lugares...\">\n          </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-col>\n      <ion-col no-padding>\n        <ion-card [hidden]=\"!selected_page\">\n          <ion-card-header padding>\n            <ion-card-title>\n              <strong>{{selected_page?.title}}</strong>\n            </ion-card-title>\n          </ion-card-header>\n          <hr>\n          <ion-card-content [innerHTML]=\"selected_page?.body\" padding>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row justify-content-center padding-vertical>\n      <ion-col padding-vertical size=\"auto\">\n        <img width=\"200px\" height=\"auto\" src=\"./assets/img/support.png\">\n        <br />\n        <ion-text text-center color=\"dark\">\n          <h1>¿Todavía dudas?</h1>\n        </ion-text>\n        <ion-text text-center color=\"medium_2\">\n          <p>Tus dudas no la encontrastes en Preguntas Frecuentes.<br />¡Dinos cómo podemos ayudarte!</p>\n        </ion-text>\n      </ion-col>\n    </ion-row>\n    <ion-row justify-content-center>\n      <ion-col size=\"auto\">\n        <ion-button (click)=\"openChat()\" color=\"danger\">Chatear con soporte</ion-button>\n        <img id=\"hi\" src=\"./assets/img/hi.png\">\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class=\"bg\">\n    <ion-grid padding-vertical fixed>\n      <ion-row justify-content-center>\n        <ion-col no-padding size=\"auto\">\n          <ion-text color=\"dark\">\n            Si deseas conocer las <strong>Condiciones del servicio,</strong> puedes consultarlo <ion-text\n              color=\"danger\">\n              aquí</ion-text>.\n          </ion-text>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <app-footer></app-footer>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/support/support.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host hr {\n  background: var(--ion-color-border, #AAAAAA); }\n\n:host app-footer {\n  background: rgba(93, 93, 93, 0.1);\n  border: none; }\n\n:host .page {\n  --padding-start: 0px;\n  --inner-padding-start: 0px; }\n\n:host .page ion-icon {\n    background: var(--ion-color-danger);\n    color: var(--ion-color-light);\n    border-radius: 6px;\n    padding: 5px;\n    margin: 0;\n    margin-right: 8px;\n    width: 16px;\n    height: 16px; }\n\n:host img {\n  display: block;\n  margin: auto; }\n\n:host ion-col {\n  position: relative; }\n\n:host #hi {\n  position: absolute;\n  display: block;\n  width: 130px;\n  height: auto;\n  bottom: 50%;\n  left: 100%; }\n\n:host .bg {\n  background: rgba(var(--ion-color-danger-rgb), 0.3); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvc3VwcG9ydC9zdXBwb3J0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLDRDQUE0QyxFQUFBOztBQUZwRDtFQU1RLGlDQUFpQztFQUNqQyxZQUFZLEVBQUE7O0FBUHBCO0VBV1Esb0JBQWdCO0VBQ2hCLDBCQUFzQixFQUFBOztBQVo5QjtJQWVZLG1DQUFtQztJQUNuQyw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixTQUFTO0lBQ1QsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxZQUFZLEVBQUE7O0FBdEJ4QjtFQTJCUSxjQUFjO0VBQ2QsWUFBWSxFQUFBOztBQTVCcEI7RUFnQ1Esa0JBQWtCLEVBQUE7O0FBaEMxQjtFQW9DUSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLFlBQVk7RUFDWixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVUsRUFBQTs7QUF6Q2xCO0VBNkNRLGtEQUFrRCxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc3VwcG9ydC9zdXBwb3J0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBociB7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1ib3JkZXIsICNBQUFBQUEpO1xuICAgIH1cblxuICAgIGFwcC1mb290ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDkzLCA5MywgOTMsIDAuMSk7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICB9XG5cbiAgICAucGFnZSB7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLWlubmVyLXBhZGRpbmctc3RhcnQ6IDBweDtcblxuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyKTtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA4cHg7XG4gICAgICAgICAgICB3aWR0aDogMTZweDtcbiAgICAgICAgICAgIGhlaWdodDogMTZweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGltZyB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgfVxuXG4gICAgaW9uLWNvbCB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB9XG5cbiAgICAjaGkge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMTMwcHg7XG4gICAgICAgIGhlaWdodDogYXV0bztcbiAgICAgICAgYm90dG9tOiA1MCU7XG4gICAgICAgIGxlZnQ6IDEwMCU7XG4gICAgfVxuXG4gICAgLmJnIHtcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSh2YXIoLS1pb24tY29sb3ItZGFuZ2VyLXJnYiksIDAuMyk7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/support/support.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/support/support.page.ts ***!
  \***********************************************/
/*! exports provided: SupportPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportPage", function() { return SupportPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modals_chat_support_chat_support_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modals/chat-support/chat-support.component */ "./src/app/modals/chat-support/chat-support.component.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _seo_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../seo.service */ "./src/app/seo.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var SupportPage = /** @class */ (function () {
    function SupportPage(modalController, router, pageService, seo) {
        var _this = this;
        this.modalController = modalController;
        this.router = router;
        this.pageService = pageService;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_4__["Pagination"]();
        this.pages = [];
        this.selected_page = null;
        this.router.paramMap.subscribe(function (params) {
            _this.page_id = Number(params.get("page_id"));
            if (_this.page_id == 1) {
                seo.addMetagsSite('Rentto | Términos y condiciones', 'Términos y condiciones - Tus dudas no la encontrastes en Preguntas Frecuentes. ¡Dinos cómo podemos ayudarte!', 'términos, condiciones, email, facebook, rentar, convierte, dinero, Rentto, extra, lugares, parqueadero, parking, almacenamiento, domestico, equipaje');
            }
            if (_this.page_id == 2) {
                seo.addMetagsSite('Rentto | Políticas de Privacidad', 'Políticas de Privacidad - Tus dudas no la encontrastes en Preguntas Frecuentes. ¡Dinos cómo podemos ayudarte!', 'políticas, Privacidad, email, facebook, rentar, convierte, dinero, Rentto, extra, lugares, parqueadero, parking, almacenamiento, domestico, equipaje');
            }
            _this.selected_page = _this.pages.find(function (page) { return page.id == _this.page_id; });
        });
    }
    SupportPage.prototype.ngOnInit = function () {
        this.list();
    };
    SupportPage.prototype.openChat = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modals_chat_support_chat_support_component__WEBPACK_IMPORTED_MODULE_2__["ChatSupportComponent"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SupportPage.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.pages = response.models;
            _this.pagination = response.pagination;
            _this.selected_page = (_this.page_id) ? _this.pages.find(function (page) { return page.id == _this.page_id; }) : _this.pages[0];
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    SupportPage.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.pages = _this.pages.concat(response.models);
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    SupportPage.prototype.load = function (callback) {
        this.pageService.list({
            page: this.pagination.page,
            limit: 30,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], SupportPage.prototype, "infiniteScroll", void 0);
    SupportPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-support',
            template: __webpack_require__(/*! ./support.page.html */ "./src/app/pages/support/support.page.html"),
            styles: [__webpack_require__(/*! ./support.page.scss */ "./src/app/pages/support/support.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _services__WEBPACK_IMPORTED_MODULE_3__["PageService"], _seo_service__WEBPACK_IMPORTED_MODULE_6__["SeoService"]])
    ], SupportPage);
    return SupportPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-support-support-module.js.map