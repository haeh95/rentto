(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./src/app/models/auth.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/auth.model.ts ***!
  \**************************************/
/*! exports provided: Auth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Auth", function() { return Auth; });
var Auth = /** @class */ (function () {
    function Auth(device) {
        this.device = device;
        this.fingerprint = this.device.uuid || "qwerty";
        this.os = this.device.platform || "qwerty";
        this.platform = this.device.platform || "qwerty";
    }
    return Auth;
}());



/***/ }),

/***/ "./src/app/models/chat.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/chat.model.ts ***!
  \**************************************/
/*! exports provided: Chat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Chat", function() { return Chat; });
var Chat = /** @class */ (function () {
    function Chat() {
    }
    return Chat;
}());



/***/ }),

/***/ "./src/app/models/city.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/city.model.ts ***!
  \**************************************/
/*! exports provided: City */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "City", function() { return City; });
var City = /** @class */ (function () {
    function City() {
    }
    return City;
}());



/***/ }),

/***/ "./src/app/models/comment.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/comment.model.ts ***!
  \*****************************************/
/*! exports provided: Comment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return Comment; });
var Comment = /** @class */ (function () {
    function Comment() {
    }
    return Comment;
}());



/***/ }),

/***/ "./src/app/models/favorite.model.ts":
/*!******************************************!*\
  !*** ./src/app/models/favorite.model.ts ***!
  \******************************************/
/*! exports provided: Favorite */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Favorite", function() { return Favorite; });
var Favorite = /** @class */ (function () {
    function Favorite() {
    }
    return Favorite;
}());



/***/ }),

/***/ "./src/app/models/filter.model.ts":
/*!****************************************!*\
  !*** ./src/app/models/filter.model.ts ***!
  \****************************************/
/*! exports provided: Filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Filter", function() { return Filter; });
var Filter = /** @class */ (function () {
    function Filter() {
        this.limit = 20;
    }
    return Filter;
}());



/***/ }),

/***/ "./src/app/models/index.ts":
/*!*********************************!*\
  !*** ./src/app/models/index.ts ***!
  \*********************************/
/*! exports provided: Auth, City, Chat, Comment, Favorite, Filter, Message, Page, Pagination, Photo, PlaceFilter, Place, QueryFilter, Question, Register, Reserve, Review, Service, User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.model */ "./src/app/models/auth.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Auth", function() { return _auth_model__WEBPACK_IMPORTED_MODULE_0__["Auth"]; });

/* harmony import */ var _city_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./city.model */ "./src/app/models/city.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "City", function() { return _city_model__WEBPACK_IMPORTED_MODULE_1__["City"]; });

/* harmony import */ var _chat_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat.model */ "./src/app/models/chat.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Chat", function() { return _chat_model__WEBPACK_IMPORTED_MODULE_2__["Chat"]; });

/* harmony import */ var _comment_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./comment.model */ "./src/app/models/comment.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return _comment_model__WEBPACK_IMPORTED_MODULE_3__["Comment"]; });

/* harmony import */ var _favorite_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./favorite.model */ "./src/app/models/favorite.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Favorite", function() { return _favorite_model__WEBPACK_IMPORTED_MODULE_4__["Favorite"]; });

/* harmony import */ var _filter_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filter.model */ "./src/app/models/filter.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Filter", function() { return _filter_model__WEBPACK_IMPORTED_MODULE_5__["Filter"]; });

/* harmony import */ var _message_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./message.model */ "./src/app/models/message.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return _message_model__WEBPACK_IMPORTED_MODULE_6__["Message"]; });

/* harmony import */ var _page_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page.model */ "./src/app/models/page.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Page", function() { return _page_model__WEBPACK_IMPORTED_MODULE_7__["Page"]; });

/* harmony import */ var _pagination_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pagination.model */ "./src/app/models/pagination.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Pagination", function() { return _pagination_model__WEBPACK_IMPORTED_MODULE_8__["Pagination"]; });

/* harmony import */ var _photo_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./photo.model */ "./src/app/models/photo.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Photo", function() { return _photo_model__WEBPACK_IMPORTED_MODULE_9__["Photo"]; });

/* harmony import */ var _place_filter_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./place-filter.model */ "./src/app/models/place-filter.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlaceFilter", function() { return _place_filter_model__WEBPACK_IMPORTED_MODULE_10__["PlaceFilter"]; });

/* harmony import */ var _place_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./place.model */ "./src/app/models/place.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Place", function() { return _place_model__WEBPACK_IMPORTED_MODULE_11__["Place"]; });

/* harmony import */ var _query_filter_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./query-filter.model */ "./src/app/models/query-filter.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QueryFilter", function() { return _query_filter_model__WEBPACK_IMPORTED_MODULE_12__["QueryFilter"]; });

/* harmony import */ var _question_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./question.model */ "./src/app/models/question.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Question", function() { return _question_model__WEBPACK_IMPORTED_MODULE_13__["Question"]; });

/* harmony import */ var _register_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./register.model */ "./src/app/models/register.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return _register_model__WEBPACK_IMPORTED_MODULE_14__["Register"]; });

/* harmony import */ var _reverse_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./reverse.model */ "./src/app/models/reverse.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Reserve", function() { return _reverse_model__WEBPACK_IMPORTED_MODULE_15__["Reserve"]; });

/* harmony import */ var _review_model__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./review.model */ "./src/app/models/review.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Review", function() { return _review_model__WEBPACK_IMPORTED_MODULE_16__["Review"]; });

/* harmony import */ var _service_model__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./service.model */ "./src/app/models/service.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Service", function() { return _service_model__WEBPACK_IMPORTED_MODULE_17__["Service"]; });

/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./user.model */ "./src/app/models/user.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _user_model__WEBPACK_IMPORTED_MODULE_18__["User"]; });






















/***/ }),

/***/ "./src/app/models/message.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/message.model.ts ***!
  \*****************************************/
/*! exports provided: Message */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return Message; });
var Message = /** @class */ (function () {
    function Message() {
    }
    return Message;
}());



/***/ }),

/***/ "./src/app/models/page.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/page.model.ts ***!
  \**************************************/
/*! exports provided: Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Page", function() { return Page; });
var Page = /** @class */ (function () {
    function Page() {
    }
    return Page;
}());



/***/ }),

/***/ "./src/app/models/pagination.model.ts":
/*!********************************************!*\
  !*** ./src/app/models/pagination.model.ts ***!
  \********************************************/
/*! exports provided: Pagination */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pagination", function() { return Pagination; });
var Pagination = /** @class */ (function () {
    function Pagination() {
        this.page = 1;
    }
    return Pagination;
}());



/***/ }),

/***/ "./src/app/models/photo.model.ts":
/*!***************************************!*\
  !*** ./src/app/models/photo.model.ts ***!
  \***************************************/
/*! exports provided: Photo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Photo", function() { return Photo; });
var Photo = /** @class */ (function () {
    function Photo() {
    }
    return Photo;
}());



/***/ }),

/***/ "./src/app/models/place-filter.model.ts":
/*!**********************************************!*\
  !*** ./src/app/models/place-filter.model.ts ***!
  \**********************************************/
/*! exports provided: PlaceFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlaceFilter", function() { return PlaceFilter; });
var PlaceFilter = /** @class */ (function () {
    function PlaceFilter() {
        this.limit = 20;
    }
    return PlaceFilter;
}());



/***/ }),

/***/ "./src/app/models/place.model.ts":
/*!***************************************!*\
  !*** ./src/app/models/place.model.ts ***!
  \***************************************/
/*! exports provided: Place */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Place", function() { return Place; });
/* harmony import */ var _photo_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./photo.model */ "./src/app/models/photo.model.ts");

var Place = /** @class */ (function () {
    function Place() {
        this.is_favorite = false;
        this.photos = [];
        this.services = [];
        this.thumbnail = new _photo_model__WEBPACK_IMPORTED_MODULE_0__["Photo"]();
    }
    return Place;
}());



/***/ }),

/***/ "./src/app/models/query-filter.model.ts":
/*!**********************************************!*\
  !*** ./src/app/models/query-filter.model.ts ***!
  \**********************************************/
/*! exports provided: QueryFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueryFilter", function() { return QueryFilter; });
var QueryFilter = /** @class */ (function () {
    function QueryFilter() {
        this.status = null;
        this.query = null;
        this.type = 1;
        this.sort = "rank_value";
        this.price = 0;
        this.start_at = null;
        this.duration = 1;
        this.latitude = null;
        this.longitude = null;
    }
    return QueryFilter;
}());



/***/ }),

/***/ "./src/app/models/question.model.ts":
/*!******************************************!*\
  !*** ./src/app/models/question.model.ts ***!
  \******************************************/
/*! exports provided: Question */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question", function() { return Question; });
var Question = /** @class */ (function () {
    function Question() {
    }
    return Question;
}());



/***/ }),

/***/ "./src/app/models/register.model.ts":
/*!******************************************!*\
  !*** ./src/app/models/register.model.ts ***!
  \******************************************/
/*! exports provided: Register */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return Register; });
var Register = /** @class */ (function () {
    function Register(device) {
        this.device = device;
        this.fingerprint = this.device.uuid || "qwerty";
        this.os = this.device.platform || "qwerty";
        this.platform = this.device.platform || "qwerty";
    }
    return Register;
}());



/***/ }),

/***/ "./src/app/models/reverse.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/reverse.model.ts ***!
  \*****************************************/
/*! exports provided: Reserve */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reserve", function() { return Reserve; });
var Reserve = /** @class */ (function () {
    function Reserve() {
        this.id = null;
        this.from = null;
        this.to = null;
        this.status = null;
    }
    return Reserve;
}());



/***/ }),

/***/ "./src/app/models/review.model.ts":
/*!****************************************!*\
  !*** ./src/app/models/review.model.ts ***!
  \****************************************/
/*! exports provided: Review */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Review", function() { return Review; });
var Review = /** @class */ (function () {
    function Review() {
    }
    return Review;
}());



/***/ }),

/***/ "./src/app/models/service.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/service.model.ts ***!
  \*****************************************/
/*! exports provided: Service */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Service", function() { return Service; });
var Service = /** @class */ (function () {
    function Service() {
    }
    return Service;
}());



/***/ }),

/***/ "./src/app/models/user.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/user.model.ts ***!
  \**************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
        this.id = null;
        this.profile_photo = null;
        this.name = null;
        this.email = null;
        this.phone = null;
        this.role = null;
        this.my_places = 0;
        this.create_place = 1;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/pages/chat/chat.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.module.ts ***!
  \*******************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat.page */ "./src/app/pages/chat/chat.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_5__["ChatPage"]
    }
];
var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_5__["ChatPage"]]
        })
    ], ChatPageModule);
    return ChatPageModule;
}());



/***/ }),

/***/ "./src/app/pages/chat/chat.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-toolbar>\n  <ion-buttons class=\"ion-hide-md-up\" slot=\"start\">\n    <ion-button (click)=\"goBack()\" fill=\"clear\" color=\"medium_2\">\n      <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n  <ion-text text-center color=\"medium_2\">\n    <p>{{user_to_show.name}}</p>\n  </ion-text>\n</ion-toolbar>\n<ion-content padding>\n  <ion-infinite-scroll position=\"top\" threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas chats...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <ion-row nowrap *ngFor=\"let message of messages\">\n    <ion-col [offset]=\"(message.from_id == user.id)?7:0\" size=\"5\">\n      <p class=\"message\" [ngClass]=\"{ 'me': message.from_id == user.id }\" no-margin>{{message.message}}</p>\n      <p no-margin>{{message.created_at | date}}</p>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <form #messageForm=\"ngForm\" (ngSubmit)=\"send()\" novalidate>\n    <ion-item lines=\"none\" color=\"messageBar\">\n      <ion-input [(ngModel)]=\"text\" name=\"text\" (keyup.enter)=\"messageForm.onSubmit()\" placeholder=\"Escribir un mensaje\"\n        required></ion-input>\n      <ion-icon slot=\"end\" src=\"./assets/svg/camera.svg\"></ion-icon>\n    </ion-item>\n  </form>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block;\n  position: relative;\n  height: 100%; }\n  :host ion-content {\n    height: calc(100% - 106px); }\n  :host ion-content p.message {\n      border-radius: 5px;\n      padding: 8px;\n      background: var(--ion-color-messageBar); }\n  :host ion-content p.message.me {\n        background: var(--ion-color-primary);\n        color: var(--ion-color-light); }\n  :host ion-content p:not(.message) {\n      text-align: right;\n      font-size: 12px;\n      color: var(--ion-color-medium_2);\n      margin-top: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvY2hhdC9jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsWUFBWSxFQUFBO0VBSGhCO0lBTVEsMEJBQTBCLEVBQUE7RUFObEM7TUFXZ0Isa0JBQWtCO01BQ2xCLFlBQVk7TUFDWix1Q0FBdUMsRUFBQTtFQWJ2RDtRQWdCb0Isb0NBQW9DO1FBQ3BDLDZCQUE2QixFQUFBO0VBakJqRDtNQXNCZ0IsaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixnQ0FBZ0M7TUFDaEMsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEwNnB4KTtcblxuICAgICAgICBwIHtcblxuICAgICAgICAgICAgJi5tZXNzYWdlIHtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogOHB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tZXNzYWdlQmFyKTtcblxuICAgICAgICAgICAgICAgICYubWUge1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJjpub3QoLm1lc3NhZ2UpIHtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW1fMik7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/chat/chat.page.ts ***!
  \*****************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, route, chatService) {
        this.navCtrl = navCtrl;
        this.route = route;
        this.chatService = chatService;
        this.user = JSON.parse(localStorage.getItem("user") || '{}');
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_5__["Pagination"]();
    }
    ChatPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParamMap.subscribe(function (params) {
            _this.user_to_show = new _models__WEBPACK_IMPORTED_MODULE_5__["User"]();
            params.keys.forEach(function (key) {
                if (_this.user_to_show.hasOwnProperty(key))
                    _this.user_to_show[key] = params.get(key);
            });
        });
        this.route.paramMap.subscribe(function (params) {
            _this.chat_id = params.get("chat_id");
            _this.list();
        });
    };
    ChatPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ChatPage.prototype.send = function () {
        var _this = this;
        if (this.messageForm.form.valid)
            this.chatService.message(this.chat_id, this.text).subscribe(function (response) {
                _this.messages.push(response);
                _this.text = "";
            });
    };
    ChatPage.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.messages = response.models;
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatPage.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.messages = _this.messages.concat(response.models);
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    ChatPage.prototype.load = function (callback) {
        this.chatService.messages(this.chat_id, {
            page: this.pagination.page,
            limit: 20,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("messageForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], ChatPage.prototype, "messageForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonInfiniteScroll"])
    ], ChatPage.prototype, "infiniteScroll", void 0);
    ChatPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chat',
            template: __webpack_require__(/*! ./chat.page.html */ "./src/app/pages/chat/chat.page.html"),
            styles: [__webpack_require__(/*! ./chat.page.scss */ "./src/app/pages/chat/chat.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services__WEBPACK_IMPORTED_MODULE_4__["ChatService"]])
    ], ChatPage);
    return ChatPage;
}());



/***/ })

}]);
//# sourceMappingURL=chat-chat-module.js.map