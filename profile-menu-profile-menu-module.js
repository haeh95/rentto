(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-menu-profile-menu-module"],{

/***/ "./src/app/pages/profile-menu/profile-menu.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/profile-menu/profile-menu.module.ts ***!
  \***********************************************************/
/*! exports provided: ProfileMenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileMenuPageModule", function() { return ProfileMenuPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_menu_router_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile-menu.router.module */ "./src/app/pages/profile-menu/profile-menu.router.module.ts");
/* harmony import */ var _profile_menu_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-menu.page */ "./src/app/pages/profile-menu/profile-menu.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var src_app_modals_modals_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modals/modals.module */ "./src/app/modals/modals.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ProfileMenuPageModule = /** @class */ (function () {
    function ProfileMenuPageModule() {
    }
    ProfileMenuPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], src_app_modals_modals_module__WEBPACK_IMPORTED_MODULE_7__["ModalsModule"], _profile_menu_router_module__WEBPACK_IMPORTED_MODULE_4__["ProfilePageRoutingModule"]],
            declarations: [_profile_menu_page__WEBPACK_IMPORTED_MODULE_5__["ProfileMenuPage"]]
        })
    ], ProfileMenuPageModule);
    return ProfileMenuPageModule;
}());



/***/ }),

/***/ "./src/app/pages/profile-menu/profile-menu.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/profile-menu/profile-menu.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid no-padding fixed>\n  <ion-row aling-items-stretch>\n    <ion-col class=\"ion-hide-md-down\" no-padding padding-top size=\"3\">\n      <ion-item lines=\"none\" margin-top color=\"noColor\">\n        <ion-label text-wrap>\n          <ion-text color=\"medium\">\n            <p no-margin>Tu</p>\n          </ion-text>\n          <h1 no-margin><strong>Perfil</strong></h1>\n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\" routerLinkActive=\"actived\" #infoStatus=\"routerLinkActive\"\n        [routerLink]=\"['/user', 'profile', 'info']\" queryParamsHandling=\"preserve\" margin-top color=\"noColor\">\n        <ion-text [color]=\"(infoStatus.isActive) ? 'dark' : 'medium'\">\n          <p no-margin><strong>Información</strong></p>\n        </ion-text>\n      </ion-item>\n      <ion-item lines=\"none\" routerLinkActive=\"actived\" #changePasswordStatus=\"routerLinkActive\"\n        [routerLink]=\"['/user', 'profile', 'change-password']\" queryParamsHandling=\"preserve\" color=\"noColor\">\n        <ion-text [color]=\"(changePasswordStatus.isActive) ? 'dark' : 'medium'\">\n          <p no-margin><strong>Cambiar contraseña</strong></p>\n        </ion-text>\n      </ion-item>\n      <ion-row align-items-center padding-horizontal justify-content-start>\n        <ion-col no-padding size=\"4\">\n          <ion-button expand=\"block\" (click)=\"logout()\" color=\"danger\">\n            Salir\n          </ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"back\" padding-horizontal align-items-center justify-content-start>\n        <ion-col no-padding size=\"9\">\n          <ion-button expand=\"block\" (click)=\"goBack()\" color=\"light\">\n            <ion-icon slot=\"start\" src=\"./assets/svg/back.svg\"></ion-icon>\n            Ver última busqueda\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n    <ion-col no-padding size=\"12\" size-md=\"8\">\n      <router-outlet></router-outlet>\n    </ion-col>\n  </ion-row>\n</ion-grid>"

/***/ }),

/***/ "./src/app/pages/profile-menu/profile-menu.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/profile-menu/profile-menu.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-grid {\n  position: relative;\n  height: calc(100% - 128px); }\n  :host ion-grid > ion-row {\n    height: 100%; }\n  :host ion-grid > ion-row ion-col[size=\"3\"] {\n      position: relative;\n      border-right: 2px solid var(--ion-color-border, #AAAAAA); }\n  :host ion-grid > ion-row ion-col[size=\"3\"] ion-item.actived {\n        border-right: 2px solid var(--ion-color-danger); }\n  :host ion-grid > ion-row ion-col[size=\"3\"] ion-row.back {\n        position: absolute;\n        bottom: 20px;\n        width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvcHJvZmlsZS1tZW51L3Byb2ZpbGUtbWVudS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxrQkFBa0I7RUFDbEIsMEJBQTBCLEVBQUE7RUFIbEM7SUFNWSxZQUFZLEVBQUE7RUFOeEI7TUFVb0Isa0JBQWtCO01BQ2xCLHdEQUF3RCxFQUFBO0VBWDVFO1FBY3dCLCtDQUErQyxFQUFBO0VBZHZFO1FBa0J3QixrQkFBa0I7UUFDbEIsWUFBWTtRQUNaLFdBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2ZpbGUtbWVudS9wcm9maWxlLW1lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGlvbi1ncmlkIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEyOHB4KTtcblxuICAgICAgICAmPmlvbi1yb3cge1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuXG4gICAgICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgICAgICAmW3NpemU9XCIzXCJdIHtcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItYm9yZGVyLCAjQUFBQUFBKTtcblxuICAgICAgICAgICAgICAgICAgICBpb24taXRlbS5hY3RpdmVkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1kYW5nZXIpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaW9uLXJvdy5iYWNrIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvdHRvbTogMjBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/profile-menu/profile-menu.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/profile-menu/profile-menu.page.ts ***!
  \*********************************************************/
/*! exports provided: ProfileMenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileMenuPage", function() { return ProfileMenuPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modals_logout_logout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modals/logout/logout.component */ "./src/app/modals/logout/logout.component.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ProfileMenuPage = /** @class */ (function () {
    function ProfileMenuPage(navCtrl, modalController, authService) {
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.authService = authService;
    }
    ProfileMenuPage.prototype.ngOnInit = function () {
    };
    ProfileMenuPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ProfileMenuPage.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal, data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modals_logout_logout_component__WEBPACK_IMPORTED_MODULE_2__["LogoutComponent"],
                            cssClass: "logout-modal"
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onDidDismiss()];
                    case 3:
                        data = (_a.sent()).data;
                        if (data) {
                            this.authService.logout().subscribe(function () {
                                localStorage.clear();
                                _this.navCtrl.navigateRoot("home");
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileMenuPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-menu',
            template: __webpack_require__(/*! ./profile-menu.page.html */ "./src/app/pages/profile-menu/profile-menu.page.html"),
            styles: [__webpack_require__(/*! ./profile-menu.page.scss */ "./src/app/pages/profile-menu/profile-menu.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _services__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], ProfileMenuPage);
    return ProfileMenuPage;
}());



/***/ }),

/***/ "./src/app/pages/profile-menu/profile-menu.router.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/profile-menu/profile-menu.router.module.ts ***!
  \******************************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_menu_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-menu.page */ "./src/app/pages/profile-menu/profile-menu.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: "",
        component: _profile_menu_page__WEBPACK_IMPORTED_MODULE_2__["ProfileMenuPage"],
        children: [
            {
                path: "info",
                children: [
                    {
                        path: "",
                        loadChildren: "../profile/profile.module#ProfilePageModule"
                    }
                ]
            },
            {
                path: "change-password",
                children: [
                    { path: "", loadChildren: "../change-password/change-password.module#ChangePasswordPageModule" }
                ]
            },
            {
                path: "",
                redirectTo: "/user/profile/info",
                pathMatch: "full"
            }
        ]
    },
    {
        path: "",
        redirectTo: "/user/profile/info",
        pathMatch: "full"
    }
];
var ProfilePageRoutingModule = /** @class */ (function () {
    function ProfilePageRoutingModule() {
    }
    ProfilePageRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProfilePageRoutingModule);
    return ProfilePageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=profile-menu-profile-menu-module.js.map