(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"],{

/***/ "./src/app/pages/history/history.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/history/history.module.ts ***!
  \*************************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./history.page */ "./src/app/pages/history/history.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_5__["HistoryPage"]
    }
];
var HistoryPageModule = /** @class */ (function () {
    function HistoryPageModule() {
    }
    HistoryPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_history_page__WEBPACK_IMPORTED_MODULE_5__["HistoryPage"]]
        })
    ], HistoryPageModule);
    return HistoryPageModule;
}());



/***/ }),

/***/ "./src/app/pages/history/history.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/history/history.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid no-padding fixed>\n  <ion-row aling-items-stretch>\n    <ion-col class=\"ion-hide-md-down\" no-padding padding-top size=\"3\">\n      <ion-item lines=\"none\" margin-top color=\"noColor\">\n        <ion-label text-wrap>\n          <ion-text color=\"medium\">\n            <p no-margin>Dónde has estado</p>\n          </ion-text>\n          <h1 no-margin><strong>Historial</strong></h1>\n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\" class=\"actived\" margin-top color=\"noColor\">\n        <ion-text color=\"dark\">\n          <p no-margin><strong>Lista</strong></p>\n        </ion-text>\n      </ion-item>\n    </ion-col>\n    <ion-col no-padding size=\"12\" size-md=\"8\">\n      <ion-toolbar class=\"ion-hide-md-up\" padding-top>\n        <ion-buttons slot=\"start\">\n          <ion-button (click)=\"goBack()\" fill=\"clear\" color=\"medium_2\">\n            <ion-icon slot=\"icon-only\" src=\"./assets/svg/back.svg\"></ion-icon>\n          </ion-button>\n          <ion-label>\n            <ion-text color=\"medium\">\n              <h1 no-margin>Dónde has estado</h1>\n            </ion-text>\n            <ion-text color=\"dark\">\n              <h1 no-margin><strong>Historial</strong></h1>\n            </ion-text>\n          </ion-label>\n        </ion-buttons>\n      </ion-toolbar>\n      <ion-content padding>\n        <ion-row padding>\n          <ion-col *ngFor=\"let reserve of reserves\" size=\"12\" size-md=\"4\">\n            <app-reserve [reserve]=\"reserve\"></app-reserve>\n          </ion-col>\n        </ion-row>\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMore($event)\">\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando mas lugares...\">\n          </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-content>\n    </ion-col>\n  </ion-row>\n</ion-grid>"

/***/ }),

/***/ "./src/app/pages/history/history.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/history/history.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-grid {\n  position: relative; }\n  :host ion-grid ion-row {\n    height: 100%; }\n  :host ion-grid ion-row ion-col[size=\"3\"] {\n      border-right: 2px solid var(--ion-color-border, #AAAAAA); }\n  :host ion-grid ion-row ion-col[size=\"3\"] ion-item.actived {\n        border-right: 2px solid var(--ion-color-danger); }\n  :host ion-toolbar {\n  padding-left: 8px;\n  padding-right: 8px;\n  --padding-left: 8px;\n  --padding-right: 8px; }\n  @media (max-width: 820px) {\n  :host ion-content ion-row {\n    padding-left: 0;\n    padding-right: 0;\n    --padding-left: 0;\n    --padding-right: 0; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGtCQUFrQixFQUFBO0VBRjFCO0lBS1ksWUFBWSxFQUFBO0VBTHhCO01BU29CLHdEQUF3RCxFQUFBO0VBVDVFO1FBWXdCLCtDQUErQyxFQUFBO0VBWnZFO0VBb0JRLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsbUJBQWU7RUFDZixvQkFBZ0IsRUFBQTtFQUloQjtFQTNCUjtJQTZCZ0IsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixpQkFBZTtJQUNmLGtCQUFnQixFQUFBLEVBQ25CIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24tZ3JpZCB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICBpb24tcm93IHtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcblxuICAgICAgICAgICAgaW9uLWNvbCB7XG4gICAgICAgICAgICAgICAgJltzaXplPVwiM1wiXSB7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1ib3JkZXIsICNBQUFBQUEpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlvbi1pdGVtLmFjdGl2ZWQge1xuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWRhbmdlcik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpb24tdG9vbGJhciB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogOHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1sZWZ0OiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1yaWdodDogOHB4O1xuICAgIH1cblxuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDgyMHB4KSB7XG4gICAgICAgICAgICBpb24tcm93IHtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctbGVmdDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctcmlnaHQ6IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/history/history.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/history/history.page.ts ***!
  \***********************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HistoryPage = /** @class */ (function () {
    function HistoryPage(navCtrl, reserveService) {
        this.navCtrl = navCtrl;
        this.reserveService = reserveService;
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_3__["Pagination"]();
    }
    HistoryPage.prototype.ngOnInit = function () {
        this.list();
    };
    HistoryPage.prototype.list = function () {
        var _this = this;
        this.load(function (response) {
            _this.reserves = response.models;
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    HistoryPage.prototype.loadMore = function (event) {
        var _this = this;
        this.pagination.page++;
        this.load(function (response) {
            event.target.complete();
            _this.reserves = _this.reserves.concat(response.models);
            _this.pagination = response.pagination;
            if (_this.pagination.page < _this.pagination.pages) {
                _this.infiniteScroll.disabled = false;
            }
            else {
                _this.infiniteScroll.disabled = true;
            }
        });
    };
    HistoryPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    HistoryPage.prototype.load = function (callback) {
        this.reserveService.list({
            page: this.pagination.page,
            limit: 12,
            filters: null,
            sort: null
        }).subscribe(callback);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], HistoryPage.prototype, "infiniteScroll", void 0);
    HistoryPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-history',
            template: __webpack_require__(/*! ./history.page.html */ "./src/app/pages/history/history.page.html"),
            styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/pages/history/history.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _services__WEBPACK_IMPORTED_MODULE_2__["ReserveService"]])
    ], HistoryPage);
    return HistoryPage;
}());



/***/ })

}]);
//# sourceMappingURL=history-history-module.js.map