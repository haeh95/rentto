<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rentto_blog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12qwaszx' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U[[PJU0Zw(fyK75.qo{v0ecs~_2mJ1bt^Ps>Xo/PM)uBp>cUl^ciRK6aI3azDXXe' );
define( 'SECURE_AUTH_KEY',  'fNYtyNi_/t#|u%`z9Jur^QoNy~[*GVh<:^d6Ea}+P%%[_!(W9Mut,y4:E}XT|5%x' );
define( 'LOGGED_IN_KEY',    'yK1ISn3^Kq+Yx:ChoUBryye}C#l)9,+[&og*H7AH;q?fVIE7Bi~NP0.>k-BYl!}&' );
define( 'NONCE_KEY',        '(*-;^Fj3yu)C$m%B^G?KZs?$:j-px} L#8A&b&:F=f>D*@Bs^zaA|/RQKzF9WeuD' );
define( 'AUTH_SALT',        'Z43W|`wtED,1dQ!=ErV~d@Go/kTK-D2u7c.f#R`2u$rV<(nQr(}1K6v$X=`1&[GM' );
define( 'SECURE_AUTH_SALT', '!(}-l,v=l{9x`!yLXUQZ$.<2P,!7J4c:^pv}~im/HYiZ2P?S!d~d=dd~}Or}?uwG' );
define( 'LOGGED_IN_SALT',   'ZydAv5rI5(34th*Ws-$!#GFFjyAv/!]_}(buWq*pmfWm;KO;X&Ze82/yBH=X/hcO' );
define( 'NONCE_SALT',       'eJ(~:Y7+RZfMw&nj-`KN?Y6!Eu}!SnY`sc`Cd{Sum-])_$x)2O,n[kbp*ZViBd9L' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
//#try_files $uri /index.html;     
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
?>