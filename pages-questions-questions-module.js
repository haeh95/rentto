(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-questions-questions-module"],{

/***/ "./src/app/pages/questions/questions.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/questions/questions.module.ts ***!
  \*****************************************************/
/*! exports provided: QuestionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsPageModule", function() { return QuestionsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _questions_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./questions.page */ "./src/app/pages/questions/questions.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _questions_page__WEBPACK_IMPORTED_MODULE_5__["QuestionsPage"]
    }
];
var QuestionsPageModule = /** @class */ (function () {
    function QuestionsPageModule() {
    }
    QuestionsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_questions_page__WEBPACK_IMPORTED_MODULE_5__["QuestionsPage"]]
        })
    ], QuestionsPageModule);
    return QuestionsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/questions/questions.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/questions/questions.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header-dark></app-header-dark>\n<ion-content padding-top>\n  <ion-grid padding-top fixed>\n    <ion-row padding-bottom nowrap>\n      <ion-col no-padding size=\"12\" size-md=\"4\">\n        <ion-item id=\"place\" lines=\"none\" color=\"light\">\n          <ion-thumbnail slot=\"start\">\n              <div class=\"image\" [style.backgroundImage]=\"'url(' + (place.photos[0]?.url || './assets/img/default_1.png') + ')'\"></div>\n          </ion-thumbnail>\n          <ion-label>\n            <ion-text color=\"dark\">\n              <h1 no-margin><strong>{{place.name}}</strong></h1>\n            </ion-text>\n            <ion-text color=\"danger\">\n              <p no-margin>${{place.price | number:'1.0-0'}}/{{place.price_duration}}</p>\n            </ion-text>\n          </ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <hr />\n  <ion-grid padding-vertical fixed>\n    <ion-row padding-bottom align-items-center>\n      <ion-col>\n        <ion-text color=\"dark\">\n          <p><strong>Preguntas y respuestas</strong></p>\n        </ion-text>\n        <form #questionForm=\"ngForm\" (ngSubmit)=\"ask()\" novalidate>\n          <ion-row [hidden]=\"!isLogin()\">\n            <ion-col no-padding>\n              <ion-input placeholder=\"Realiza tu pregunta\" [(ngModel)]=\"question\" name=\"question\" required></ion-input>\n            </ion-col>\n            <ion-col size=\"auto\"></ion-col>\n            <ion-col size=\"auto\" no-padding>\n              <ion-button type=\"submit\" no-margin color=\"danger\">Preguntar</ion-button>\n            </ion-col>\n          </ion-row>\n        </form>\n        <br />\n        <ion-row align-items-center justify-content-between>\n          <ion-col size=\"auto\">\n            <ion-row align-items-center>\n              <ion-text color=\"medium_2\">\n                <p><strong>Mostrando {{(pagination.page-1)*pagination.pageSize}}-{{pagination.page*pagination.pageSize}}\n                    de {{pagination.total}} preguntas</strong></p>\n              </ion-text>\n            </ion-row>\n          </ion-col>\n          <ion-col size=\"auto\">\n            <ion-row align-items-center>\n              <ion-col size=\"auto\">\n                <ion-text color=\"medium_2\"><strong>Ordenar por</strong></ion-text>\n              </ion-col>\n              <ion-col>\n                <ion-select interface=\"popover\" [(ngModel)]=\"filter.sort\" (ionChange)=\"list()\" name=\"order_by\"\n                  no-padding placeholder=\"Seleccion una opción\">\n                  <ion-select-option value=\"-created_at\"><strong>Mas recientes</strong></ion-select-option>\n                  <ion-select-option value=\"created_at\"><strong>Mas antiguos</strong></ion-select-option>\n                </ion-select>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <div padding-vertical>\n          <app-question [question]=\"question\" *ngFor=\"let question of questions\"></app-question>\n        </div>\n        <app-pagination [url]=\"'/questions/' + place_id\" [page]=\"pagination.page\" [total]=\"pagination.pages\">\n        </app-pagination>\n      </ion-col>\n      <ion-col class=\"ion-hide-md-down\" no-padding size=\"4\"></ion-col>\n    </ion-row>\n  </ion-grid>\n  <app-footer></app-footer>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/questions/questions.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/questions/questions.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host app-footer {\n  background: rgba(93, 93, 93, 0.1);\n  border: none; }\n\n:host hr {\n  background: var(--ion-color-border, #AAAAAA); }\n\n:host ion-input {\n  border: 1px solid #F0F1F3;\n  border-radius: 8px;\n  width: 100%; }\n\n:host ion-item#place {\n  border: 1px solid #F0F1F3;\n  border-radius: 8px;\n  --border-radius: 8px; }\n\n:host div.image {\n  display: block;\n  width: 100%;\n  height: 100%;\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0RvY3VtZW50cy9zaXRpb3MvcmVudHRvL3NyYy9hcHAvcGFnZXMvcXVlc3Rpb25zL3F1ZXN0aW9ucy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFUSxpQ0FBaUM7RUFDakMsWUFBWSxFQUFBOztBQUhwQjtFQU9RLDRDQUE0QyxFQUFBOztBQVBwRDtFQVdRLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsV0FBVyxFQUFBOztBQWJuQjtFQWtCWSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLG9CQUFnQixFQUFBOztBQXBCNUI7RUF5QlEsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLDRCQUE0QjtFQUM1QiwyQkFBMkIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3F1ZXN0aW9ucy9xdWVzdGlvbnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGFwcC1mb290ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDkzLCA5MywgOTMsIDAuMSk7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICB9XG5cbiAgICBociB7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1ib3JkZXIsICNBQUFBQUEpO1xuICAgIH1cblxuICAgIGlvbi1pbnB1dCB7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMEYxRjM7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAmI3BsYWNlIHtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMEYxRjM7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRpdi5pbWFnZSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/questions/questions.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/questions/questions.page.ts ***!
  \***************************************************/
/*! exports provided: QuestionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsPage", function() { return QuestionsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services */ "./src/app/services/index.ts");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment/moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment_moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment_moment__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var QuestionsPage = /** @class */ (function () {
    function QuestionsPage(placeService, route, questionService) {
        this.placeService = placeService;
        this.route = route;
        this.questionService = questionService;
        this.filter = new _models__WEBPACK_IMPORTED_MODULE_3__["Filter"]();
        this.place_id = null;
        this.place = new _models__WEBPACK_IMPORTED_MODULE_3__["Place"]();
        this.pagination = new _models__WEBPACK_IMPORTED_MODULE_3__["Pagination"]();
        this.filter.limit = 6;
        this.filter.sort = "-created_at";
    }
    QuestionsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParamMap.subscribe(function (params) {
            _this.queryFilter = new _models__WEBPACK_IMPORTED_MODULE_3__["QueryFilter"]();
            params.keys.forEach(function (key) {
                _this.queryFilter[key] = params.get(key);
            });
            if (_this.place_id && _this.queryFilter)
                _this.get();
        });
        this.route.paramMap.subscribe(function (params) {
            _this.place_id = Number(params.get("place_id"));
            _this.pagination.page = _this.filter.page = Number(params.get("page"));
            if (_this.place_id)
                _this.list();
            if (_this.place_id && _this.queryFilter)
                _this.get();
        });
    };
    QuestionsPage.prototype.ask = function () {
        var _this = this;
        if (this.questionForm.form.valid)
            this.questionService.add(this.place.id, { question: this.question }).subscribe(function () { return _this.list(); });
    };
    QuestionsPage.prototype.isLogin = function () {
        return !!localStorage.getItem("token");
    };
    QuestionsPage.prototype.get = function () {
        var _this = this;
        this.placeService.get(this.place_id, { latitude: this.queryFilter.latitude, longitude: this.queryFilter.longitude, from: this.queryFilter.start_at, to: moment_moment__WEBPACK_IMPORTED_MODULE_5__(this.queryFilter.start_at).add(this.queryFilter.duration, (this.queryFilter.type == 4) ? "hour" : "month").format("YYYY-MM-DD[T]HH:00") }).subscribe(function (response) {
            _this.place = response;
        });
    };
    QuestionsPage.prototype.list = function () {
        var _this = this;
        this.questionService.list(this.place_id, this.filter).subscribe(function (response) {
            _this.questions = response.models;
            _this.pagination = response.pagination;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("questionForm"),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"])
    ], QuestionsPage.prototype, "questionForm", void 0);
    QuestionsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-questions',
            template: __webpack_require__(/*! ./questions.page.html */ "./src/app/pages/questions/questions.page.html"),
            styles: [__webpack_require__(/*! ./questions.page.scss */ "./src/app/pages/questions/questions.page.scss")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_4__["PlaceService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services__WEBPACK_IMPORTED_MODULE_4__["QuestionService"]])
    ], QuestionsPage);
    return QuestionsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-questions-questions-module.js.map